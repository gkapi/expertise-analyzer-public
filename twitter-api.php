<?php

//Using TwitterOauth library, https://twitteroauth.com/.
//Built by Abraham Williams for use with the Twitter API. TwitterOAuth is not affiliated Twitter, Inc.
require "twitteroauth/autoload.php";

use Abraham\TwitterOAuth\TwitterOAuth;
include "twitter-app-config.php";

function getTwitterData($screen_name,$languages){
	
	//Change of max execution time because in some cases exceed the current time limit.
	ini_set('max_execution_time', 800);
	
	//Dictionary from http://www.computerhope.com/jargon/program.htm
	
	$file = fopen("cs-dictionary.txt","r");
	$i=0;
	while($line=fgets($file)){
		//Eliminate last position of line that is the new line and we dont want to save it.
		$word=strtoupper(substr($line,0,strlen($line)-2));
		$dictionary[$i]=$word;
		$i++;
	}
	fclose($file);
	
						
						
	//Create twwets by language , retwwets by language, likes by language arrays.
	$languages_keys = array_keys($languages);
	$tweets_by_language;
	$retweets_by_language;
	$likes_by_language;
	foreach($languages_keys as $k){
		$tweets_by_language[$k]=0;
		$retweets_by_language[$k]=0;
		$likes_by_language[$k]=0;
	
	}
	
	//Create an application-authorized connection to Twitter API.
	$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET);
	$content = $connection->get("account/verify_credentials");


	$page=1;
	//Use tweet_id for field max_id to avoid get duplicate tweets, if new tweets created during API call.
	$tweet_id;
	
	
	//This method can only return up to 3,200 of a user’s most recent Tweets. Max count per page = 200.
	/***PROBLEM HERE****/
	$statuses = $connection->get("statuses/user_timeline", array("screen_name" => $screen_name, "count" => 200, "page"=>$page));
	/***PROBLEM HERE****/
	
	
	//$followers = $statuses[0]->user->followers_count;
	$followers="";
	
	do{
		
		//Check if a user exists.
		//If user doesnt exist API returns an object with the proper message.
		//If user exists API returns an array with user's tweets.
		if(gettype($statuses)=='object'){
			if(property_exists($statuses, 'errors')){
				return false;
			}
		}
		
		//Check only user's tweets, not retweeted tweets.
		for($i=0;$i<count($statuses);$i++){
			$status=$statuses[$i];
			
			if($followers==""){
				$followers = $status->user->followers_count;
			}
			
			$tweet_id= $status->id_str;
			$tweet_str= strtoupper($status->text);
			//split twwet by " "
			$tweet_str=explode(" ",$tweet_str);
			$retweeted=$status->retweeted;
			
			for($j=0;$j<count($languages_keys);$j++){
				
				$lang=$languages_keys[$j];
				//Check if tweet contains any words from cs dictionary.
				$inDictionary=0;
				for($k=0;$k<count($tweet_str);$k++){
					
					if(in_array($tweet_str[$k],$dictionary)!=false){
						$inDictionary++;
					}
					
					if( ($retweeted==false) && (in_array($lang,$tweet_str)!=false) && $inDictionary>=2 ){
					
						$tweets_by_language[$lang]++;
						$retweets_by_language[$lang]+=$status->retweet_count;
						$likes_by_language[$lang]+=$status->favorite_count;
					}
				
				}
				
				
	
			}
			
		}
		
		$page++;
		
	}while($statuses = $connection->get("statuses/user_timeline", array("screen_name" => $screen_name, "count" => 200, "page"=>$page,"max_id"=>$tweet_id)));

	return array('followers'=>$followers,'tweets_by_language'=>$tweets_by_language,'retweets_by_language'=>$retweets_by_language,
				'likes_by_language'=>$likes_by_language);
	
}


?>