<?php
//Database Connection*********************************************************
	//The analyze is false when user has last updated his profile 5 days ago.
	$analyze=false;
	$conn = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);
	if($conn->connect_error){
		die("Connection failed: ".mysql_connect_error());
	}
	
	// $sql_user="SELECT last_update FROM user WHERE github_username=".$github_username.
	// " OR stackoverflow_id=".$stackoverflow_id." OR twitter_username=".$twitter_username;
	// $rows_user=mysqli_query($conn,$sql_user);
	// $row_user=mysqli_fetch_assoc($rows_user);
	
	// Database********************************************************************************
	
	//If all accounts are invalid dont do anything.
	if($github_account || $stackoverflow_account || $twitter_account || $bitbucket_account){
		$db_id;
		$db_github=null;
		$db_stack=null;
		$db_twitter=null;
		$db_bitbucket=null;
		$db_last_update;
		$exist=false;
		//Today's Date.
		$now = date("Y-m-d");
		$sql;
		
		//Check if github,stackoverflow or twitter account exists in DB
		$sql="SELECT * FROM expertanalyzer.user WHERE github_username='".$github_username."' OR
		stackoverflow_id='".$stackoverflow_id."' OR 
		twitter_username='".$twitter_username."' OR
		bitbucket_username='".$bitbucket_username."'";
		//Execute query.
		$rows=mysqli_query($conn,$sql);
		
		
		//If user exists in DB add any new account to DB.
		$num_rows = mysqli_num_rows($rows);
		if($num_rows>0){
			
			$exist=true;
			$row = mysqli_fetch_assoc($rows);
			$db_id= $row['id'];
			$db_github=$row['github_username'];
			$db_stack=$row['stackoverflow_id'];
			$db_twitter=$row['twitter_username'];
			$db_bitbucket=$row['bitbucket_username'];
			//last_update is the first time that user joined the system.
			$db_last_update=$row['last_update'];
			
			//If github account doesn't exists and user gave a valid github account, insert it in DB.
			if($db_github==null && $github_account){
				$sql="UPDATE `expertanalyzer`.`user` SET `github_username`='".$github_username."' WHERE id='".$db_id."'";
				$rows=mysqli_query($conn,$sql);
			}
			//If stackoverflow account doesn't exists and user gave a valid stackoverflow account, insert it in DB.
			if($db_stack==null && $stackoverflow_account){
				$sql="UPDATE `expertanalyzer`.`user` SET `stackoverflow_id`='".$stackoverflow_id."' WHERE id='".$db_id."'";
				$rows=mysqli_query($conn,$sql);
			}
			//If twitter account doesn't exists and user gave a valid twitter account, insert it in DB.
			if($db_twitter==null && $twitter_account){
				$sql="UPDATE `expertanalyzer`.`user` SET `twitter_username`='".$twitter_username."' WHERE id='".$db_id."'";
				$rows=mysqli_query($conn,$sql);
			}
			//If bitbucket account doesn't exists and user gave a valid bitbucket account, insert it in DB.
			if($db_bitbucket==null && $bitbucket_account){
				$sql="UPDATE `expertanalyzer`.`user` SET `bitbucket_username`='".$bitbucket_username."' WHERE id='".$db_id."'";
				$rows=mysqli_query($conn,$sql);
			}
			
		}
		//User doesn't exist in DB.
		else{
			
			$sql="INSERT INTO `expertanalyzer`.`user` (`github_username`, `stackoverflow_id`, `twitter_username`, `last_update`, `bitbucket_username`)
			VALUES ('".$github_username."', '".intval($stackoverflow_id)."', '".$twitter_username."', '".$now."', '".$bitbucket_username."')";
			$rows=mysqli_query($conn,$sql);
			$db_id=mysqli_insert_id($conn);
			//If any of the accounts is not valid update them to null.
			if(!$github_account){
				$sql="UPDATE `expertanalyzer`.`user` SET `github_username`=null WHERE id='".$db_id."'";
				$rows=mysqli_query($conn,$sql);
			}
			if(!$stackoverflow_account){
				$sql="UPDATE `expertanalyzer`.`user` SET `stackoverflow_id`=null WHERE id='".$db_id."'";
				$rows=mysqli_query($conn,$sql);
			}
			if(!$twitter_account){
				$sql="UPDATE `expertanalyzer`.`user` SET `twitter_username`=null WHERE id='".$db_id."'";
				$rows=mysqli_query($conn,$sql);
			}
			if(!$bitbucket_account){
				$sql="UPDATE `expertanalyzer`.`user` SET `bitbucket_username`=null WHERE id='".$db_id."'";
				$rows=mysqli_query($conn,$sql);
			}
		}
		
		//if user exists, update him in DB if only if last update > 5days.
		// $difference_obj = date_diff(date_create($now),date_create($db_last_update));
		// $update_difference_days = intval($difference_obj->days);
		
		//Insert Github Data to DB.
		if($github_account){
			if($db_github!=null){
				//If account exists, first delete the current data and then insert.
				$sql="DELETE FROM `expertanalyzer`.`github_user` WHERE `username`='".$db_github."'";
				$rows=mysqli_query($conn,$sql);
				$sql="DELETE FROM `expertanalyzer`.`github` WHERE `username`='".$db_github."'";
				$rows=mysqli_query($conn,$sql);
				$sql="DELETE FROM `expertanalyzer`.`github_commits` WHERE `username`='".$db_github."'";
				$rows=mysqli_query($conn,$sql);
			}
			//Insert number of repositories by language.
			foreach($repos_by_language as $k => $v){
				//If there are languages with 0 repositories, continue.
				if($v==0)
					continue;
				$sql="INSERT INTO `expertanalyzer`.`github` (`username`, `language`, `projects`) VALUES ('".$github_username."', '".$k."', '".$v."')";
				$rows=mysqli_query($conn,$sql);
			}
			//Insert number of commits by year and by language.
			foreach($commits_by_year as $k => $v){
				foreach($v as $kk =>$vv){
				
					//If there are languages with 0 commits, continue.
					if($vv==0)
						continue;
					$sql="INSERT INTO `expertanalyzer`.`github_commits` (`username`, `language`, `year`, `commits`) VALUES ('".$github_username."', '".$kk."', ".$k.", '".$vv."')";
					$rows=mysqli_query($conn,$sql);
				}
				
			}
			
			//Insert user's github data.
			$sql="INSERT INTO `expertanalyzer`.`github_user` (`username`, `followers`, `involvement_days`, `account_days`) VALUES ('".$github_username."', '".$github_followers."', '".$involvement_duration."', '".$account_duration."')";
			$rows=mysqli_query($conn,$sql);
			
		}
		
		//Insert Stackoverflow Data to DB.
		if($stackoverflow_account){
			if($db_stack!=null){
				//If account exists, first delete the current data and then insert.
				$sql="DELETE FROM `expertanalyzer`.`stackoverflow_user` WHERE `user_id`='".$stackoverflow_id."'";
				$rows=mysqli_query($conn,$sql);
				$sql="DELETE FROM `expertanalyzer`.`stackoverflow` WHERE `user_id`='".$stackoverflow_id."'";
				$rows=mysqli_query($conn,$sql);
			}
			//Insert number of answers by language.
			foreach($answers_about_languages as $k => $v){
				//If there are languages with 0 answers, continue.
				if($v==0)
					continue;
				$sql="INSERT INTO `expertanalyzer`.`stackoverflow` (`user_id`, `language`, `answers`, `upvotes`, `badge`)VALUES ('".$stackoverflow_id."', '".$k."', '".$v."', '".$answers_upvotes_about_languages[$k]."', '".$badges_about_languages[$k]."')";
				$rows=mysqli_query($conn,$sql);
			}
			
			
			//Insert user's stackoverflow data.
			$sql="INSERT INTO `expertanalyzer`.`stackoverflow_user` (`user_id`, `gold_badges`, `silver_badges`, `bronze_badges`, `reputation`) VALUES ('".$stackoverflow_id."', '".$gold_badges."', '".$silver_badges."', '".$bronze_badges."', '".$reputation."')";
			$rows=mysqli_query($conn,$sql);
		}
		
		//Insert Twitter Data to DB.
		if($twitter_account){
			if($db_github!=null){
				//If account exists, first delete the current data and then insert.
				$sql="DELETE FROM `expertanalyzer`.`twitter_user` WHERE `username`='".$twitter_username."'";
				$rows=mysqli_query($conn,$sql);
				$sql="DELETE FROM `expertanalyzer`.`twitter` WHERE `username`='".$twitter_username."'";
				$rows=mysqli_query($conn,$sql);
			}
			//Insert number of tweets by language.
			foreach($tweets_by_language as $k => $v){
				//If there are languages with 0 tweets, continue.
				if($v==0)
					continue;
				$sql="INSERT INTO `expertanalyzer`.`twitter` (`username`, `language`, `tweets`, `retweets`, `likes`) VALUES ('".$twitter_username."', '".$k."', '".$v."', '".$retweets_by_language[$k]."', '".$likes_by_language[$k]."')";
				$rows=mysqli_query($conn,$sql);
			}
			$sql="INSERT INTO `expertanalyzer`.`twitter_user` (`username`, `followers`) VALUES ('".$twitter_username."', '".$twitter_followers."')";
			$rows=mysqli_query($conn,$sql);
		}
		//Insert BitBucket Data to DB.
		if($bitbucket_account){
			if($db_bitbucket!=null){
				//If account exists, first delete the current data and then insert.
				$sql="DELETE FROM `expertanalyzer`.`bitbucket_user` WHERE `username`='".$db_bitbucket."'";
				$rows=mysqli_query($conn,$sql);
				$sql="DELETE FROM `expertanalyzer`.`bitbucket` WHERE `username`='".$db_bitbucket."'";
				$rows=mysqli_query($conn,$sql);
				$sql="DELETE FROM `expertanalyzer`.`bitbucket_commits` WHERE `username`='".$db_bitbucket."'";
				$rows=mysqli_query($conn,$sql);
			}
			//Insert number of repositories by language.
			foreach($bitbucket_repos_by_language as $k => $v){
				//If there are languages with 0 repositories, continue.
				if($v==0)
					continue;
				$sql="INSERT INTO `expertanalyzer`.`bitbucket` (`username`, `language`, `projects`) VALUES ('".$bitbucket_username."', '".$k."', '".$v."')";
				$rows=mysqli_query($conn,$sql);
			}
			//Insert number of commits by year and by language.
			foreach($bitbucket_commits_by_year as $k => $v){
				foreach($v as $kk =>$vv){
				
					//If there are languages with 0 commits, continue.
					if($vv==0)
						continue;
					$sql="INSERT INTO `expertanalyzer`.`bitbucket_commits` (`username`, `language`, `year`, `commits`) VALUES ('".$bitbucket_username."', '".$kk."', ".$k.", '".$vv."')";
					$rows=mysqli_query($conn,$sql);
					
				}
				
			}
			//Insert user's bitbucket data.
			$sql="INSERT INTO `expertanalyzer`.`bitbucket_user` (`username`, `followers`) VALUES ('".$bitbucket_username."', '".$bitbucket_followers."')";
			$rows=mysqli_query($conn,$sql);
			
		}
		
	}
	//END Database*****************************************************************************

?>