<?php

//Get Stack Overflow data from DB
function getDataStackOverflowDB($stackoverflow_id){
	$data=null;
	$conn = mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);
	
	//Retrieve data from table stackoverflow_user
	$sql="SELECT * FROM expertanalyzer.stackoverflow_user WHERE user_id='".$stackoverflow_id."'";
	$rows=mysqli_query($conn,$sql);
	
	$reputation;
	$gold_badges;
	$silver_badges;
	$bronze_badges;
	while($row=mysqli_fetch_assoc($rows)){
		$reputation=intval($row['reputation']);
		$gold_badges=intval($row['gold_badges']);
		$silver_badges=intval($row['silver_badges']);
		$bronze_badges=intval($row['bronze_badges']);
		
	}
	
	//Retrieve data from table stackoverflow
	$sql="SELECT * FROM expertanalyzer.stackoverflow WHERE user_id='".$stackoverflow_id."'";
	$rows=mysqli_query($conn,$sql);
	
	$badges;
	$answers;
	$answers_upvotes;
	while($row=mysqli_fetch_assoc($rows)){
		$language=$row['language'];
		$badges[$language]=$row['badge'];
		$answers[$language]=intval($row['answers']);
		$answers_upvotes[$language]=intval($row['upvotes']);
		
	}
	
	
	//Close DB connection
	mysqli_close($conn);
	
	$data['reputation']=$reputation;
	$data['gold_badges']=$gold_badges;
	$data['silver_badges']=$silver_badges;
	$data['bronze_badges']=$bronze_badges;
	
	
	$data['badges']=$badges;
	$data['answers']=$answers;
	$data['answers_upvotes']=$answers_upvotes;
	
	
	
	return $data;
	
}

?>