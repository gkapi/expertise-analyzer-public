<?php

if($stackoverflow_data){
	$_SESSION['stackoverflow_id']=$stackoverflow_id;
	$stackoverflow_account=true;
	$reputation=$stackoverflow_data['reputation'];
	$bronze_badges=$stackoverflow_data['bronze_badges'];
	$silver_badges=$stackoverflow_data['silver_badges'];
	$gold_badges=$stackoverflow_data['gold_badges'];
	
	
	
	//$badges is an associative array that contains the type of badges for each language. 
	$badges_about_languages=$stackoverflow_data['badges'];
	//$answers is an associative array that contains the number of answers for each language. 
	$answers_about_languages=$stackoverflow_data['answers'];
	//$answers_upvotes is an associative array that contains the number of upvotes for each answer about a language. 
	$answers_upvotes_about_languages=$stackoverflow_data['answers_upvotes'];
	
	
	
	//Assign values to session.
	$_SESSION['reputation']=$reputation;
	$_SESSION['bronze_badges']=$bronze_badges;
	$_SESSION['silver_badges']=$silver_badges;
	$_SESSION['gold_badges']=$gold_badges;
	
	//if badges_about_languages exist
	if($badges_about_languages){
		foreach($badges_about_languages as $k => $v)
			if($v!=''){
				$_SESSION['badges_about_languages']=$badges_about_languages;
				break;
			}
	}
	
	
	if($answers_about_languages){
		//Sort tables
		arsort($answers_about_languages,true);
		//if is not empty
		foreach($answers_about_languages as $k => $v)
			if($v>0){
				$_SESSION['answers_about_languages']=$answers_about_languages;
				break;
			}
		//Calculate percentages for each project's language of total repos.
		$answers_counter=0;
		foreach($answers_about_languages as $k => $v){
				$answers_counter+=$v;
		}
		$answers_percentages = new ArrayObject($answers_about_languages);
		
		$answers_keys = array_keys($answers_about_languages);
		//$percentages=$bitbucket_repos_by_language->getArrayCopy();
		/*if($answers_counter!=0){
			for($i=0;$i<count($answers_percentages);$i++){
				$answers_percentages[$answers_keys[$i]]=intval($answers_percentages[$answers_keys[$i]]/$answers_counter*100);
			}
		}
		
		
		$_SESSION['stack_graph1']="<script>$(function() {
			Morris.Donut({
				element: 'stack_graph1',
				data: [";
				for($i=0;$i<count($answers_percentages);$i++){
					if($answers_percentages[$answers_keys[$i]]==0){
						continue;
					}
					$_SESSION['stack_graph1'].=" { label: '".$answers_keys[$i]."', value: ".$answers_percentages[$answers_keys[$i]]."  }";
					
					if($i<count($answers_percentages)-1){
						$_SESSION['stack_graph1'].=", ";
					}
					
				}
				$_SESSION['stack_graph1'].="
					],
					 formatter: function (x) { return x + '%'},
				resize: true
			});
			});
			</script>";	
			*/
			
		$low_percentages=0;
		$others_pointer=0;
		if($answers_counter!=0){
			for($i=0;$i<count($answers_percentages);$i++){
				$answers_percentages[$answers_keys[$i]]=number_format($answers_percentages[$answers_keys[$i]]/$answers_counter*100, 2, '.', '');
				//Languages with percentages < 2%.
				if($answers_percentages[$answers_keys[$i]]<2){
					$low_percentages+=$answers_percentages[$answers_keys[$i]];
					if($others_pointer==0){
						$others_pointer=$i;
					}
				}
			}
		}
		
		$_SESSION['stack_graph1']="<script>$(function() {
			Morris.Donut({
				element: 'stack_graph1',
				data: [";
				for($i=0;$i<count($answers_percentages);$i++){
					if($others_pointer>0 && $i==$others_pointer){
						$_SESSION['stack_graph1'].=" { label: 'Others', value: ".$low_percentages."  }";
						break;
					}
					else{
						if($answers_percentages[$answers_keys[$i]]==0){
							continue;
						}
						$_SESSION['stack_graph1'].=" { label: '".$answers_keys[$i]."', value: ".$answers_percentages[$answers_keys[$i]]."  }";
						
						if($i<count($answers_percentages)-1){
							$_SESSION['stack_graph1'].=", ";
						}
					}
					
				}
				$_SESSION['stack_graph1'].="
					],
					 formatter: function (x) { return x + '%'},
				resize: true
			});
			});
			</script>";	
			
			
			$_SESSION['total_answers']=0;
			foreach($answers_about_languages as $k => $v)
				$_SESSION['total_answers']+=$v;
				
	}
	
	
	//if answers_upvotes_about_languages exists
	if($answers_upvotes_about_languages){
		//sort table
		arsort($answers_upvotes_about_languages,true);
		//if is not empty
		foreach($answers_upvotes_about_languages as $k => $v)
			if($v>0){
				$_SESSION['answers_upvotes_about_languages']=$answers_upvotes_about_languages;
				break;
			}
		
		
		
		
			
			
			
		//Calculate percentages for each project's language of total repos.
		$upvotes_counter=0;
		foreach($answers_upvotes_about_languages as $k => $v){
				$upvotes_counter+=$v;
		}
		$upvotes_percentages = new ArrayObject($answers_upvotes_about_languages);
		
		$upvotes_keys = array_keys($answers_upvotes_about_languages);
		//$percentages=$bitbucket_repos_by_language->getArrayCopy();
		
		$low_percentages=0;
		$others_pointer=0;
		if($upvotes_counter!=0){
			for($i=0;$i<count($upvotes_percentages);$i++){
				$upvotes_percentages[$upvotes_keys[$i]]=number_format($upvotes_percentages[$upvotes_keys[$i]]/$upvotes_counter*100, 2, '.', '');
				//Languages with percentages < 2%.
				if($upvotes_percentages[$upvotes_keys[$i]]<2){
					$low_percentages+=$upvotes_percentages[$upvotes_keys[$i]];
					if($others_pointer==0){
						$others_pointer=$i;
					}
				}
			}
		}
		
		$_SESSION['stack_graph2']="<script>$(function() {
			Morris.Donut({
				element: 'stack_graph2',
				data: [";
				for($i=0;$i<count($upvotes_percentages);$i++){
					if($others_pointer>0 && $i==$others_pointer){
						$_SESSION['stack_graph2'].=" { label: 'Others', value: ".$low_percentages."  }";
						break;
					}
					else{
						if($upvotes_percentages[$upvotes_keys[$i]]==0){
							continue;
						}
						$_SESSION['stack_graph2'].=" { label: '".$upvotes_keys[$i]."', value: ".$upvotes_percentages[$upvotes_keys[$i]]."  }";
						
						if($i<count($upvotes_percentages)-1){
							$_SESSION['stack_graph2'].=", ";
						}
					}
					
				}
				$_SESSION['stack_graph2'].="
					],
					 formatter: function (x) { return x + '%'},
				resize: true
			});
			});
			</script>";
		
		/*if($upvotes_counter!=0){
			for($i=0;$i<count($upvotes_percentages);$i++){
				$upvotes_percentages[$upvotes_keys[$i]]=intval($upvotes_percentages[$upvotes_keys[$i]]/$upvotes_counter*100);
			}
		}
		
		$_SESSION['stack_graph2']="<script>$(function() {
			Morris.Donut({
				element: 'stack_graph2',
				data: [";
				for($i=0;$i<count($upvotes_percentages);$i++){
					if($upvotes_percentages[$upvotes_keys[$i]]==0){
						continue;
					}
					$_SESSION['stack_graph2'].=" { label: '".$upvotes_keys[$i]."', value: ".$upvotes_percentages[$upvotes_keys[$i]]."  }";
					
					if($i<count($upvotes_percentages)-1){
						$_SESSION['stack_graph2'].=", ";
					}
					
				}
				$_SESSION['stack_graph2'].="
					],
					 formatter: function (x) { return x + '%'},
				resize: true
			});
			});
			</script>";
			*/
			$_SESSION['total_upvotes']=0;
			foreach($answers_upvotes_about_languages as $k => $v)
				$_SESSION['total_upvotes']+=$v;
	}
}

?>