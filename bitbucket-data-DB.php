<?php
//Get bitbucket data from DB
function get_bitbucket_dataDB($bitbucket_username){
	$data=null;
	$conn = mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);
	
	//Retrieve data from table bitbucket
	$sql="SELECT * FROM expertanalyzer.bitbucket WHERE username='".$bitbucket_username."'";
	$rows=mysqli_query($conn,$sql);
	
	
	
	//Repositories counter
	$repositories_counter=0;
	//Repositories by language
	$repos_by_language=null;
	while($row=mysqli_fetch_assoc($rows)){
		$language=$row['language'];
		$projects=$row['projects'];
		
		//Count repositories by language
		$repos_by_language[$language]=intval($projects);
		//Count repositories
		$repositories_counter+=$projects;
		
	}
	//Retrieve data from table bitbucket_user
	$sql="SELECT * FROM expertanalyzer.bitbucket_user WHERE username='".$bitbucket_username."'";
	$rows=mysqli_query($conn,$sql);
	
	$followers=null;
	
	while($row=mysqli_fetch_assoc($rows)){
		
		$followers=intval($row['followers']);
		
	}
	
	//Retrieve data from table bitbucket_commits
	$sql="SELECT * FROM expertanalyzer.bitbucket_commits WHERE username='".$bitbucket_username."'";
	$rows=mysqli_query($conn,$sql);	
	
	//Initialize array commits_by_year
	$languages_keys=array_keys($repos_by_language);
	$commits_by_year;
	for($j=2009;$j<=2016;$j++){
		for($i=0;$i<count($languages_keys);$i++){
			$commits_by_year[$j][$languages_keys[$i]]=0;
		}
	}
	//Get commits by year by language
	while($row=mysqli_fetch_assoc($rows)){
		$language=$row['language'];
		$year=$row['year'];
		$commits=$row['commits'];
		
		$commits_by_year[$year][$language]=intval($commits);
	}
	
	//Close DB connection
	mysqli_close($conn);
	
	$data['repositories_counter']=$repositories_counter;
	$data['repos_by_language']=$repos_by_language;
	$data['followers']=$followers;
	$data['commits']=$commits_by_year;
	
	
	
	
	return $data;
	
}

?>