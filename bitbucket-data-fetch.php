<?php

	if($bitbucket_data!=null){
		
		$_SESSION['bitbucket_username']=$bitbucket_username;
		$bitbucket_account=true;
		
		$bitbucket_followers =$bitbucket_data['followers'];
		$bitbucket_repos=$bitbucket_data['repositories_counter'];
		$bitbucket_repos_by_language=$bitbucket_data['repos_by_language'];
		$bitbucket_commits_by_year=$bitbucket_data['commits'];
		
		//If repositories exist.
		if($bitbucket_repos>0){

			//Associative array keys.
			$bitbucket_keys = array_keys($bitbucket_repos_by_language);
			//Count repos with a specific language. Not empty.
			$bitbucket_repos_counter=0;
			for($i=0;$i<count($bitbucket_repos_by_language);$i++){
				$bitbucket_repos_counter +=intval($bitbucket_repos_by_language[$bitbucket_keys[$i]]);
			}
			//Calculate percentages for each project's language of total repos.
			$bitbucket_percentages = new ArrayObject($bitbucket_repos_by_language);
			//$percentages=$bitbucket_repos_by_language->getArrayCopy();
			if($bitbucket_repos_counter!=0){
				for($i=0;$i<count($bitbucket_percentages);$i++){
					$bitbucket_percentages[$bitbucket_keys[$i]]=number_format(doubleval($bitbucket_percentages[$bitbucket_keys[$i]]/$bitbucket_repos_counter*100),2);
				}
			}
			
			//Check if $bitbucket_commits_by_year contains any commits.
			$createBitbucketCommits=false;
			foreach($bitbucket_commits_by_year as $k => $v){
				foreach($v as $kk => $vv)
					if($vv!=0){
						
						$createBitbucketCommits=true;
						break;
					}
			}
			
			
			foreach($bitbucket_repos_by_language as $k => $v){
				if($v!=0){
					$createBitbucketRepos=true;
					break;
				}
			}
			
			
			//Associative array keys.
			$commits_year_keys = array_keys($bitbucket_commits_by_year);
			$language_commits_keys=array_keys($bitbucket_commits_by_year['2009']);
		
			if($createBitbucketCommits){
				//A string to know which language has any commits.
				
				$_SESSION['bitbucket_commits_graph']= "<script>$(function() {
			
				Morris.Area({
					element: 'bitbucket_commits_graph',
					data: [
					";
					for($i=0;$i<count($commits_year_keys);$i++){
						
						$_SESSION['bitbucket_commits_graph'].=
						" {period: '".$commits_year_keys[$i]." ', ";
						
						for($j=0;$j<count($language_commits_keys);$j++){
							//If there are'nt any commits, continue.
							
							$_SESSION['bitbucket_commits_graph'].="'".$language_commits_keys[$j]."' : ".$bitbucket_commits_by_year[$commits_year_keys[$i]][$language_commits_keys[$j]];
							//If value==0 print null.
							// if($bitbucket_commits_by_year[$commits_year_keys[$i]][$language_commits_keys[$j]]==0){
								// $_SESSION['bitbucket_commits_graph'].="null ";
							// }
							// else{
								// $_SESSION['bitbucket_commits_graph'].=$bitbucket_commits_by_year[$commits_year_keys[$i]][$language_commits_keys[$j]];
							// }
							
							//Keep the languages that has any commits.
							
							//Only for the top 4 languages.
							if(!($j==4 || $j==(count($language_commits_keys)-1) )){
								$_SESSION['bitbucket_commits_graph'].=",";
							}
							
						}
						$_SESSION['bitbucket_commits_graph'].="
							  }";
						if($i!=(count($commits_year_keys)-1)){
							$_SESSION['bitbucket_commits_graph'].=" , ";
						}
					}
					
					$_SESSION['bitbucket_commits_graph'].="],
					xkey: 'period',
					ykeys: [";
					for($i=0;$i<count($language_commits_keys);$i++){
						if($i==5)
							break;
						if($i==4 || $i==(count($language_commits_keys)-1) )
							$_SESSION['bitbucket_commits_graph'].="'".$language_commits_keys[$i]."'";
						else
							$_SESSION['bitbucket_commits_graph'].="'".$language_commits_keys[$i]."',";
					}	
					$_SESSION['bitbucket_commits_graph'].="],
					labels: [";
					for($i=0;$i<count($language_commits_keys);$i++){
						if($i==5)
							break;
						if($i==4 || $i==(count($language_commits_keys)-1) )
							$_SESSION['bitbucket_commits_graph'].="'".$language_commits_keys[$i]."'";
						else
							$_SESSION['bitbucket_commits_graph'].="'".$language_commits_keys[$i]."',";
					}
					$_SESSION['bitbucket_commits_graph'].="],
					pointSize: 2,
					hideHover: 'auto',
					resize: true
				});
				});
				</script>";
			}
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			//Create Github graph js code*************************************************
			
			if($createBitbucketRepos){
			$_SESSION['bitbucket_percentages']="<script>$(function() {
			Morris.Donut({
				element: 'bitbucket_percentages',
				data: [";
				for($i=0;$i<count($bitbucket_percentages);$i++){
					if($bitbucket_percentages[$bitbucket_keys[$i]]==0){
						continue;
					}
					$_SESSION['bitbucket_percentages'].=" { label: '".$bitbucket_keys[$i]."', value: ".$bitbucket_percentages[$bitbucket_keys[$i]]."  }";
					
					if($i<count($bitbucket_percentages)-1){
						$_SESSION['bitbucket_percentages'].=", ";
					}
					
				}
				$_SESSION['bitbucket_percentages'].="
					],
					 formatter: function (x) { return x + '%'},
				resize: true
			});
			});
			</script>";
			}
			
			if($createBitbucketRepos){
			$_SESSION['bitbucket_repos-by-language-bar-chart']="<script>$(function() {
			Morris.Bar({
				element: 'bitbucket_repos-by-language-bar-chart',
				data: [";
				arsort($bitbucket_repos_by_language,true);
			
				$bitbucket_keys = array_keys($bitbucket_repos_by_language);
				
				for($i=0;$i<8;$i++){
					if($bitbucket_repos_by_language[$bitbucket_keys[$i]]==0){
						break;
					}
					$_SESSION['bitbucket_repos-by-language-bar-chart'].=" { language: '".$bitbucket_keys[$i]."', count: ".$bitbucket_repos_by_language[$bitbucket_keys[$i]]." }";
					
					if($i<7){
						$_SESSION['bitbucket_repos-by-language-bar-chart'].=", ";
					}
					
				}
				$_SESSION['bitbucket_repos-by-language-bar-chart'].="],
				xkey: 'language',
				ykeys: ['count'],
				labels: ['Projects'],
				hideHover: 'auto',
				resize: true
			});
			});
			</script>";
			}
			
			//END Create Bitbucket graphs js code**********************************************
			
			$_SESSION['bitbucket_repositories']=$bitbucket_repos;
		}
		else{
			$_SESSION['bitbucket_repositories']=0;
		}
		
		
		$_SESSION['bitbucket_followers']=$bitbucket_followers;
		
	}

?>