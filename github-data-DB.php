<?php
//Get GitHub data from DB
function getDataDB($github_username){
	$data=null;
	$conn = mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);
	
	//Retrieve data from table github
	$sql="SELECT * FROM expertanalyzer.github WHERE username='".$github_username."'";
	$rows=mysqli_query($conn,$sql);
	//Repositories counter
	$repositories=0;
	//Repositories by language
	$repos_by_language;
	while($row=mysqli_fetch_assoc($rows)){
		$language=$row['language'];
		$projects=$row['projects'];
		
		//Count repositories by language
		$repos_by_language[$language]=intval($projects);
		//Count repositories
		$repositories+=$projects;
		
	}
	//Retrieve data from table github_user
	$sql="SELECT * FROM expertanalyzer.github_user WHERE username='".$github_username."'";
	$rows=mysqli_query($conn,$sql);
	
	$followers;
	$account_duration;
	$involvement_duration;
	while($row=mysqli_fetch_assoc($rows)){
		
		$followers=intval($row['followers']);
		$account_duration=intval($row['account_days']);
		$involvement_duration=intval($row['involvement_days']);
		
	}
	
	//Retrieve data from table github_commits
	$sql="SELECT * FROM expertanalyzer.github_commits WHERE username='".$github_username."'";
	$rows=mysqli_query($conn,$sql);	
	
	//Initialize array commits_by_year
	$languages_keys=array_keys($repos_by_language);
	$commits_by_year;
	for($j=2009;$j<=2016;$j++){
		for($i=0;$i<count($languages_keys);$i++){
			$commits_by_year[$j][$languages_keys[$i]]=0;
		}
	}
	//Get commits by year by language
	while($row=mysqli_fetch_assoc($rows)){
		$language=$row['language'];
		$year=$row['year'];
		$commits=$row['commits'];
		
		$commits_by_year[$year][$language]=intval($commits);
	}
	
	//Close DB connection
	mysqli_close($conn);
	
	$data['repositories']=$repositories;
	$data['repos_by_language']=$repos_by_language;
	$data['followers']=$followers;
	$data['account_duration']=$account_duration;
	$data['involvement_duration']=$involvement_duration;
	$data['commits_by_year']=$commits_by_year;
	
	
	
	
	return $data;
	
}

?>