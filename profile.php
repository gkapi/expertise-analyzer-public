<?php
	session_start();
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>


    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Profile</title>
	
	
	
    <!-- BOOTSTRAP CORE STYLE CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONT AWESOME CSS -->
<link href="assets/css/font-awesome.min.css" rel="stylesheet" />
     <!-- FLEXSLIDER CSS -->
<link href="assets/css/flexslider.css" rel="stylesheet" />
    <!-- CUSTOM STYLE CSS -->
    <link href="assets/css/style.css" rel="stylesheet" />    
  <!-- Google	Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css' />
	
	<!-- MetisMenu CSS -->
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="bower_components/morrisjs/morris.css" rel="stylesheet">
	
	 <!-- DataTables CSS -->
    <link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
	
</head>
<body >
  
 <div class="navbar navbar-inverse navbar-fixed-top " id="menu">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html"><img class="logo-custom" src="assets/img/logo.png" alt=""  /></a>
            </div>
            <div class="navbar-collapse collapse move-me">
                <ul class="nav navbar-nav navbar-right">
                    <li ><a href="index.html">HOME</a></li>
                     
                    <li><a href="#user_scores">MY PROFILE</a></li>
                    
					<li><a href="https://docs.google.com/forms/d/1eIU9hv4j0PPY5TvxsAuxmw0vw3nGZPFIyEpY7VlgN_U/viewform" target="_blank">
					QUESTIONNAIRE
					</a></li>
                   
                </ul>
            </div>
           
        </div>
    </div>
      <!--NAVBAR SECTION END-->
	   <header class="header">
       <br>
	   
     </header>              
     
	 <!-- SIDEBAR SECTION-->
     
	  <div class="navbar-default sidebar" role="navigation">
	  
                <div class="row">
				
				<div class="col-md-3" id="leftCol">
				
                    <ul class="nav   affix" id="side-menu"  data-offset-top="255" data-offset-bottom="200">
						 <li>
                            <a href="#user_scores"><i class="fa fa-user fa-fw"></i> User Scores</a>
                        </li>
                        <li>
                            <a href="#github_data"><i class="fa fa-github fa-fw"></i> GitHub Data</a>
                        </li>
						 <li>
                            <a href="#bitbucket_data"><i class="fa fa-bitbucket fa-fw"></i> Bitbucket Data</a>
                        </li>
                        <li>
                            <a href="#stackoverflow_data"><i class="fa fa-stack-overflow fa-fw"></i> Stack Overflow Data</a>
                            
                        </li>
                        <li>
                            <a href="#twitter_data"><i class="fa fa-twitter fa-fw"></i> Twitter Data</a>
                        </li>
						
                       
                       
                    </ul>
                </div>
				</div>
                <!-- /.sidebar-collapse -->
            </div>
	 
	 <!-- SIDEBAR END          -->
              
           

	
	
	 <div id="page-wrapper">
            
			
					<!-- ************************* USER SCORES data***************************-->	

			<div class="row">
			<div id="user_scores" class="set-pad">
                <div class="col-lg-12">
                    <h1  class="page-header text-center">User Scores</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
			</div>
			
			<?php
			if(isset($_SESSION['not_found'])){
			?>
			
			<p align='center'>Profile is not found. Please create your profile first.</p>
			
			<?php
			}
			
			?>
			
            <div class="row">
			<?php
				if(!isset($_SESSION['repositories'])){
					//echo "<p align='center'>GitHub account not found or not provided.</p>";
				}
			?>
                <div class="col-lg-3 col-md-6">
                  
                       
                           
									<?php 
									if(!isset($_SESSION['score_popularity'])){
										$_SESSION['score_popularity']=0;
									}
											echo '
											  <div class="panel panel-primary">
											 <div class="panel-heading">
											<div class="row">
											<div class="col-xs-3">
												<i class="fa fa-users fa-5x"></i>
											</div>
											<div class="col-xs-9 text-right">
												<div class="huge">
											'.$_SESSION['score_popularity'].'%
												</div>
											<div>Overall Popularity</div>
												</div>
											</div>
											</div>
											</div>
											'; 
										
										
									
										
									?>
								
                        
                    
                    
                </div>
				
								<div class="col-lg-3 col-md-6">
                    
									<?php
										if(isset($_SESSION['system_popularity'])){
											echo '
											<div class="panel panel-yellow">
										<div class="panel-heading">
											<div class="row">
												<div class="col-xs-3">
													<i class="fa fa-users fa-5x"></i>
												</div>
												<div class="col-xs-9 text-right">
													<div class="huge">
												
											'.$_SESSION['system_popularity']."%
											
															</div>
														<div>Expertise Analyzer Popularity</div>
													</div>
												</div>
											</div>
											
										</div>
											"; 
										}
										
									
										
									?>
					
                </div>
				
                <div class="col-lg-3 col-md-6">
                    
									<?php
										if(isset($_SESSION['total_repos'])){
											echo '
											<div class="panel panel-green">
										<div class="panel-heading">
											<div class="row">
												<div class="col-xs-3">
													<i class="fa fa-folder fa-5x"></i>
												</div>
												<div class="col-xs-9 text-right">
													<div class="huge">
												
											'.$_SESSION['total_repos'].
											"
															</div>
														<div>Total Repositories</div>
													</div>
												</div>
											</div>
											
										</div>
											"; 
										}
										
									
										
									?>
					
                </div>
				

				
								<div class="col-lg-3 col-md-6">
                    
									<?php
										if(isset($_SESSION['expertise'])){
											echo '
											<div class="panel panel-'.$_SESSION['expertise_type'].'">
										<div class="panel-heading">
											<div class="row">
												<div class="col-xs-3">
													<i class="fa fa-cogs fa-5x"></i>
												</div>
												<div class="col-xs-9 text-right">
													<div class="huge">
												
											'.$_SESSION['expertise'].'%
											
															</div>
													<div>
														'.strtoupper($_SESSION['expertise_type']).'
													</div>
														<div>Expertise</div>
													</div>
												</div>
											</div>
											
										</div>
											'; 
										}
										
									
										
									?>
					
                </div>
                
			</div>

			<div class="row">
				<hr/>
				<div class="col-lg-6">
				<p>Most experienced in: <span class="highlight-cyan"><?php if(isset($_SESSION["most_experienced_in"])) 
																				echo $_SESSION["most_experienced_in"]; 
																			else
																				echo "-";?>
										</span></p>
				<p>Total commits made: <span class="highlight-black"><?php if(isset($_SESSION["total_commits"]))
																				echo $_SESSION["total_commits"]; 
																			else
																				echo "-";?>
										</span></p>
				<p>Total answers: <span class="highlight-orange"><?php if(isset($_SESSION["total_answers"]))
																			echo $_SESSION["total_answers"]; 
																		else
																			echo "-";?>
									</span></p>
				<p>Total upvotes: <span class="highlight-orange"><?php if(isset($_SESSION["total_upvotes"]))
																			echo $_SESSION["total_upvotes"];
																		else
																			echo "-";?>
									</span></p>
				<p>Joined Expertise Analyzer on: <span class="highlight-green"><?php if(isset($_SESSION["joined"]))
																						echo $_SESSION["joined"]; 
																					else
																						echo "-";?>
												</span></p>
			
				</div>
				<div class="col-lg-6">
	 
				 <?php
					if(isset($_SESSION['github_username'])){
						echo '<p><span class="highlight-black"><i class="fa fa-github fa-fw"></i> '.$_SESSION["github_username"].'</span></p>';
					}
					if(isset($_SESSION['bitbucket_username'])){
						echo '<p><span class="highlight-blue"><i class="fa fa-bitbucket fa-fw"></i> '.$_SESSION["bitbucket_username"].'</span></p>';
					}
					if(isset($_SESSION['stackoverflow_id'])){
						echo '<p><span class="highlight-orange"><i class="fa fa-stack-overflow fa-fw"></i> '.$_SESSION["stackoverflow_id"].'</span></p>';
					}
					if(isset($_SESSION['twitter_username'])){
						echo '<p><span class="highlight-azure"><i class="fa fa-twitter fa-fw"></i> '.$_SESSION["twitter_username"].'</span></p>';
					}
				 
				 ?>
				 </div>
				
				
				
			</div>
		   <!-- /.row -->
			<hr/>
			
			<div class="row">
				<?php
				if(isset($_SESSION['score_knowledge_by_language'])){
					$score_knowledge_by_language = $_SESSION['score_knowledge_by_language'];
					$skbl_keys = array_keys($score_knowledge_by_language);
					echo'
					 <div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								Knowledge(Based on Stack Overflow)
							</div>
							<!-- /.panel-heading -->
							<div class="panel-body">
								<div class="table-responsive">
									 <table class="table table-striped table-bordered table-hover" id="dataTables-knowledge">
										<thead>
											<tr>
												<th>#</th>
												<th>Language</th>
												<th>Score</th>
												
											</tr>
										</thead>
										<tbody>';
										for($i=0;$i<count($score_knowledge_by_language);$i++)
										echo'
											<tr>
												<td>'.($i+1).'</td>
												<td>'.$skbl_keys[$i].'</td>
												<td>'.number_format(($score_knowledge_by_language[$skbl_keys[$i]]*100),2).'%</td>
											   
											</tr>
										   ';
											
										echo '</tbody>
									</table>
								</div>
								<!-- /.table-responsive -->
							</div>
							<!-- /.panel-body -->
						</div>
						<!-- /.panel -->
					</div>
					<!-- /.col-lg-6 -->
					
					';
				}
				
			
				if(isset($_SESSION['system_social_activity'])){
					$system_social_activity = $_SESSION['system_social_activity'];
					$ssa_keys = array_keys($system_social_activity);
					echo'
					 <div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								Social Activity(Based on System\'s Stack Overflow and Twitter)
							</div>
							<!-- /.panel-heading -->
							<div class="panel-body">
								<div class="table-responsive">
									 <table class="table table-striped table-bordered table-hover" id="dataTables-social-activity">
										<thead>
											<tr>
												<th>#</th>
												<th>Language</th>
												<th>Score</th>
												
											</tr>
										</thead>
										<tbody>';
										for($i=0;$i<count($system_social_activity);$i++)
											if($system_social_activity[$ssa_keys[$i]]>0)
												echo'
													<tr>
														<td>'.($i+1).'</td>
														<td>'.$ssa_keys[$i].'</td>
														<td>'.$system_social_activity[$ssa_keys[$i]].'%</td>
													   
													</tr>
												   ';
											
										echo '</tbody>
									</table>
								</div>
								<!-- /.table-responsive -->
							</div>
							<!-- /.panel-body -->
						</div>
						<!-- /.panel -->
					</div>
					<!-- /.col-lg-6 -->
					
					';
				}
			
				?>
           
			</div>
            <!-- /.row -->
			

			
          <div class="row">
		  <?php
		  if(isset($_SESSION['total_repos_percentages'])){
					echo '
							<div  class="set-pad">
							<div class="set-pad">
							 <div class="panel panel-default">
						<div class="panel-heading">
							<i class="fa fa-bar-chart-o fa-fw"></i>Total Languages\' Percentage(%)
						</div>
						<div class="panel-body">
							<div id="total_repos_percentages"></div>
							
						</div>
						<!-- /.panel-body -->
						</div>
						</div>
						</div>
					';
		}
		?>
		</div>
		   <!-- /.row -->
			
<!-- *************************************END USER SCORE********************** -->
			
			
	<!-- *************************GitHub account data***************************-->		
		<div class="row">

                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
			
<!-- *************************************END GitHub Account data********************** -->


		<!-- *************************Bitbucket account data***************************-->	
			<div class="row">
			
			</div>
            <!-- /.row -->
		
           
			
<!-- *************************************END Bitbucket Account data********************** -->



<!-- *************************************Stackoverflow Account data********************** -->
			<div class="row">
			
			</div>
            <!-- /.row -->
			  <div class="row">
			  
			  
<!-- *************************************END Stackoverflow Account data***************************************** -->

			
<!-- *************************************Twitter Account data***************************************** -->
			<div class="row">
			
			</div>
            <!-- /.row -->
			 
		<!-- ***********************************END Twitter Account Data***************************************************-->	
		
		
		


		
		

		
		
		
		
			</div>
			
	<div class="row">
			
	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default" id="github_data">
    <div class="panel-heading set-pad" role="tab" id="headingOne">
	                
				
						
                    <h1 class=" page-header text-center"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
         GitHub Data
        </a></h1>
					
					
                </div>
      
	  
        
      
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
	  

	  
       <div class="row">
			<?php
				if(!isset($_SESSION['repositories'])){
					echo "<p align='center'>GitHub account not found or not provided.</p>";
				}
			?>
                <div class="col-lg-3 col-md-6">
                  
                       
                           
									<?php 
									if(isset($_SESSION['repositories'])){
											echo '
											  <div class="panel panel-primary">
											 <div class="panel-heading">
											<div class="row">
											<div class="col-xs-3">
												<i class="fa fa-folder fa-5x"></i>
											</div>
											<div class="col-xs-9 text-right">
												<div class="huge">
											'.$_SESSION['repositories'],'
												</div>
											<div>Repositories</div>
												</div>
											</div>
											</div>
											</div>
											'; 
										}
										
									
										
									?>
								
                        
                    
                    
                </div>
                <div class="col-lg-3 col-md-6">
                    
									<?php
										if(isset($_SESSION['involvement_duration'])){
											echo '
											<div class="panel panel-green">
										<div class="panel-heading">
											<div class="row">
												<div class="col-xs-3">
													<i class="fa fa-clock-o fa-5x"></i>
												</div>
												<div class="col-xs-9 text-right">
													<div class="huge">
												
											'.$_SESSION['involvement_duration'].
											"
															</div>
														<div>Involvement's Duration(days)</div>
													</div>
												</div>
											</div>
											
										</div>
											"; 
										}
										
									
										
									?>
					
                </div>
                <div class="col-lg-3 col-md-6">
                   
									<?php 
									if(isset($_SESSION['account_duration'])){
											echo '
												 <div class="panel panel-yellow">
										<div class="panel-heading">
											<div class="row">
												<div class="col-xs-3">
													<i class="fa fa-clock-o fa-5x"></i>
												</div>
												<div class="col-xs-9 text-right">
													<div class="huge">
											'.$_SESSION['account_duration'].
											"
															</div>
													<div>Account's Duration(days)</div>
													</div>
												</div>
											</div>
										 
										</div>
											"; 
										}
										
									
									?>
					
                </div>
                <div class="col-lg-3 col-md-6">
                   
									<?php 
										if(isset($_SESSION['github_followers'])){
											echo '
												 <div class="panel panel-red">
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-3">
														<i class="fa fa-users fa-5x"></i>
													</div>
													<div class="col-xs-9 text-right">
														<div class="huge">	
											'.$_SESSION['github_followers'].
											"
														</div>
														<div>Followers</div>
													</div>
												</div>
											</div>
										
										</div>
											"; 
										}
									
									?>
						
                </div>
           
			</div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12 ">
				
					<?php
					if(isset($_SESSION['github_graph']))
						echo '
						<div id="graph1" class="set-pad">
						<div class="set-pad">
						<div class="panel panel-default ">
							<div   class="panel-heading ">
								<i  class="fa fa-bar-chart-o fa-fw" ></i> Commits by year
								
							</div>
							</div>
							
							<!-- /.panel-heading -->
							<div class="panel-body">
								<div id="morris-area-chart"></div>
							</div>
							<!-- /.panel-body -->
						</div>
						</div>
						<!-- /.panel -->
						';
						
					?>
					
					<?php
					if(isset($_SESSION['github_graph2'])){
						echo '
								<div id="graph2" class="set-pad">
						<div class="set-pad">
						<div class="panel panel-default">
							<div class="panel-heading">
								<i class="fa fa-bar-chart-o fa-fw"></i>Top 7 Languages
							</div>
							<!-- /.panel-heading -->
							<div class="panel-body">
								<div class="row">
									
									<!-- /.col-lg-4 (nested) -->
									<div class="col-lg-12">
										<div id="repos-by-language-bar-chart"></div>
									</div>
									<!-- /.col-lg-8 (nested) -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.panel-body -->
						</div>
						</div>
						</div>
						<!-- /.panel -->
						
							';
					}
					?>
					<?php
						if(isset($_SESSION['github_graph3'])){
							echo '
									<div id="graph3" class="set-pad">
									<div class="set-pad">
									 <div class="panel panel-default">
								<div class="panel-heading">
									<i class="fa fa-bar-chart-o fa-fw"></i> Languages\' Percentages(%)
								</div>
								<div class="panel-body">
									<div id="morris-donut-chart"></div>
									
								</div>
								<!-- /.panel-body -->
								</div>
								</div>
								</div>
							';
						}
					?>
					
					
					
                
                <!-- /.col-lg-8 -->
					
				
					
                   
                  
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
      </div>
    </div>
  </div>
  <div class="panel panel-default" id="bitbucket_data">
    <div class="panel-heading set-pad" role="tab" id="headingTwo">
	
              
                    <h1  class="page-header text-center">
					 <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
         Bitbucket Data
        </a>
					</h1>
               
                <!-- /.col-lg-12 -->
     
      
       
      
    </div>
    <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">
         <div class="row">
			<?php
				if(!isset($_SESSION['bitbucket_repositories'])){
					echo "<p align='center'>Bitbucket account not found or not provided.</p>";
				}
			?>
                <div class="col-lg-3 col-md-6">
                  
                       <?php
										if(isset($_SESSION['bitbucket_followers'])){
											echo '
											<div class="panel panel-green">
										<div class="panel-heading">
											<div class="row">
												<div class="col-xs-3">
													<i class="fa fa-users fa-5x"></i>
												</div>
												<div class="col-xs-9 text-right">
													<div class="huge">
												
											'.$_SESSION['bitbucket_followers'].
											"
															</div>
														<div>Followers</div>
													</div>
												</div>
											</div>
											
										</div>
											"; 
										}
										
									
										
									?>
                           
									
								
                        
                    
                    
                </div>
                <div class="col-lg-3 col-md-6">
                    
							<?php 
									if(isset($_SESSION['bitbucket_repositories'])){
											echo '
											  <div class="panel panel-primary">
											 <div class="panel-heading">
											<div class="row">
											<div class="col-xs-3">
												<i class="fa fa-folder fa-5x"></i>
											</div>
											<div class="col-xs-9 text-right">
												<div class="huge">
											'.$_SESSION['bitbucket_repositories'],'
												</div>
											<div>Repositories</div>
												</div>
											</div>
											</div>
											</div>
											'; 
										}
										
									
										
									?>		
					
                </div>
               
               
           
			</div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12 ">
				
					<?php
					if(isset($_SESSION['bitbucket_commits_graph']))
						echo '
						<div id="bitbucket_graph1" class="set-pad">
						<div class="set-pad">
						<div class="panel panel-default ">
							<div   class="panel-heading ">
								<i  class="fa fa-bar-chart-o fa-fw" ></i> Commits by year
								
							</div>
							</div>
							
							<!-- /.panel-heading -->
							<div class="panel-body">
								<div id="bitbucket_commits_graph"></div>
							</div>
							<!-- /.panel-body -->
						</div>
						</div>
						<!-- /.panel -->
						';
						
					?>
					
					<?php
					if(isset($_SESSION['bitbucket_repos-by-language-bar-chart'])){
						echo '
								<div id="bitbucket_graph2" class="set-pad">
						<div class="set-pad">
						<div class="panel panel-default">
							<div class="panel-heading">
								<i class="fa fa-bar-chart-o fa-fw"></i> Top 7 Languages
							</div>
							<!-- /.panel-heading -->
							<div class="panel-body">
								<div class="row">
									
									<!-- /.col-lg-4 (nested) -->
									<div class="col-lg-12">
										<div id="bitbucket_repos-by-language-bar-chart"></div>
									</div>
									<!-- /.col-lg-8 (nested) -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.panel-body -->
						</div>
						</div>
						</div>
						<!-- /.panel -->
						
							';
					}
					
						if(isset($_SESSION['bitbucket_percentages'])){
							echo '
									<div id="bitbucket_graph3" class="set-pad">
									<div class="set-pad">
									 <div class="panel panel-default">
								<div class="panel-heading">
									<i class="fa fa-bar-chart-o fa-fw"></i> Languages Percentage(%)
								</div>
								<div class="panel-body">
									<div id="bitbucket_percentages"></div>
									
								</div>
								<!-- /.panel-body -->
								</div>
								</div>
								</div>
							';
						}
					
					?>
					
					
					
                
                <!-- /.col-lg-8 -->
					
				
					
                   
                  
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
      </div>
    </div>
  </div>
  <div class="panel panel-default" id="stackoverflow_data">
    <div class="panel-heading set-pad" role="tab" id="headingThree">
	
               
                    <h1  class="page-header text-center">
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Stack Overflow Data
        </a>
					</h1>
                
                <!-- /.col-lg-12 -->
            
      
        
      
    </div>
    <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
      	<?php
				if(!isset($_SESSION['reputation'])){
					echo "<p align='center'>Stack Overflow account not found or not provided.</p>";
				}
			?>
			<div class="row">
                <div class="col-lg-3 col-md-6">
                  
									<?php 
									
									
									if(isset($_SESSION['reputation'])){
										echo	'
											  <div class="panel panel-primary">
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-3">
														<i class="fa fa-users fa-5x"></i>
													</div>
													<div class="col-xs-9 text-right">
														<div class="huge">	
											'. $_SESSION['reputation'].
											"
															</div>
													<div>Stack Overflow Reputation</div>
													</div>
												</div>
											</div>
										
										</div>
											"; 
										}
										
									
										
									?>
					
                </div>
                <div class="col-lg-3 col-md-6">
                   
									<?php
										if(isset($_SESSION['gold_badges'])){
											echo '
											 <div class="panel panel-gold">
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-3">
														<i class="fa fa-certificate fa-5x"></i>
													</div>
													<div class="col-xs-9 text-right">
														<div class="huge">	
											'.$_SESSION['gold_badges']."
															</div>
															<div>Gold badges</div>
														</div>
													</div>
												</div>
												
											</div>
											"; 
										}
										
									
										
									?>
						
                </div>
                <div class="col-lg-3 col-md-6">
                   
									<?php 
									if(isset($_SESSION['silver_badges'])){
											echo '
											 <div class="panel panel-silver">
									<div class="panel-heading">
										<div class="row">
											<div class="col-xs-3">
												<i class="fa fa-certificate fa-5x"></i>
											</div>
											<div class="col-xs-9 text-right">
												<div class="huge">
											'.$_SESSION['silver_badges']."
														</div>
														<div>Silver badges</div>
													</div>
												</div>
											</div>
										 
										</div>
											"; 
										}
									
									
									?>
						
                </div>
                <div class="col-lg-3 col-md-6">
                    
									<?php 
										if(isset($_SESSION['bronze_badges'])){
											echo	'
											<div class="panel panel-bronze">
										<div class="panel-heading">
											<div class="row">
												<div class="col-xs-3">
													<i class="fa fa-certificate fa-5x"></i>
												</div>
												<div class="col-xs-9 text-right">
													<div class="huge">
											'. $_SESSION['bronze_badges']."
														</div>
														<div>Bronze badges</div>
													</div>
												</div>
											</div>
										
										</div>
											"; 
										}
										
									?>
						
                </div>
				</div>
				<br>
				<div class="col-lg-12">
				</div>
				<!-- Row to represent badges, answers and upvotes per language-->
				<div class="row">
				
				
				<div class="col-lg-6">
				<?php
				if(isset($_SESSION['answers_about_languages'])){
					echo '
						
						<div class="set-pad">
						<div class="panel panel-default">
								<div class="panel-heading">
									<i class="fa fa-bar-chart-o fa-fw"></i> Answers by Language(%)
								</div>
								<div class="panel-body">
									<div id="stack_graph1"></div>
									<button class="btn btn-block btn-default" data-toggle="collapse" data-target="#stack-collapse-table1">View Details</button>
								';
								
				  if(isset($_SESSION['answers_about_languages'])){
					echo'
					
                    <div class="panel panel-default collapse" id="stack-collapse-table1">
                        <div class="panel-heading">
                           Answers per Language
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-answers">
                                    <thead>
									
                                        <tr>
                                            <th>#</th>
                                            <th>Language</th>
                                            <th>Answers</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                        ';
									$table=$_SESSION['answers_about_languages'];
									$i=0;
									foreach($table as $k => $v){
										
										if($v){
											echo"
											<tr>
												<td>".($i+1)."</td>
												<td>".$k."</td>
												<td>".$v."</td>
											   
											</tr>
												";
												$i++;
										}
									}
									
				echo '					  
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
              
				';
				}
			
								
								echo'</div>
						<!-- /.panel-body -->
						</div>
						</div>
						
					';
				}
				?>
				
				</div>
				
				
				<div class="col-lg-6 ">
				<div class="set-pad">
				 <?php
				 
				 
				
				 
				 
				 if(isset($_SESSION['badges_about_languages'])){
				echo'
					
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           Top Badge per Language
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-badges">
                                    <thead>
									
                                        <tr>
                                            <th>#</th>
                                            <th>Language</th>
                                            <th>Badge</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                        ';
									$table=$_SESSION['badges_about_languages'];
									$i=0;
									foreach($table as $k => $v){
										
										if($v){
											echo"
											<tr>
												<td>".($i+1)."</td>
												<td>".$k."</td>
												<td>".$v."</td>
											   
											</tr>
												";
												$i++;
										}
									}
									
				echo '					  
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
               
           
			';
				 }
				 ?>
				 </div> <!-- div -->
				  </div>
                <!-- /.col-lg-4 -->
				 
				 
				 
				

                <!-- /.col-lg-6 -->
				
            </div>
            <!-- /.row -->
			
			<div class="row">
				
				<div class="col-lg-6 ">
				 <?php
				 
				 if(isset($_SESSION['answers_upvotes_about_languages'])){
					echo '
						<div  class="set-pad">
						<div class="set-pad">
						<div class="panel panel-default">
								<div class="panel-heading">
									<i class="fa fa-bar-chart-o fa-fw"></i> Answers\'Upvotes by Language(%)
								</div>
								<div class="panel-body">
									<div id="stack_graph2"></div>
									<button class="btn btn-block btn-default" data-toggle="collapse" data-target="#stack-collapse-table2">View Details</button>
								';
					
							if(isset($_SESSION['answers_upvotes_about_languages'])){
							echo'
							
							<div class="panel panel-default collapse" id="stack-collapse-table2">
								<div class="panel-heading">
								   Answer\'s Upvotes per Language
								</div>
								<!-- /.panel-heading -->
								<div class="panel-body">
									<div class="dataTable_wrapper">
										<table class="table table-striped table-bordered table-hover" id="dataTables-upvotes">
											<thead>
											
												<tr>
													<th>#</th>
													<th>Language</th>
													<th>Upvotes</th>
													
												</tr>
											</thead>
											<tbody>
								';
											$table=$_SESSION['answers_upvotes_about_languages'];
											$i=0;
											foreach($table as $k => $v){
												//if($i==10)
													//break;
												if($v){
													echo"
													<tr>
														<td>".($i+1)."</td>
														<td>".$k."</td>
														<td>".$v."</td>
													   
													</tr>
														";
														$i++;
												}
											}
											
						echo '					  
											</tbody>
										</table>
									</div>
									<!-- /.table-responsive -->
								</div>
								<!-- /.panel-body -->
							</div>
							<!-- /.panel -->
					  
						';
						}
					
							
					echo'</div>
						<!-- /.panel-body -->
						</div>
						</div>
						</div>
					';
				}
				 
				  
			?>
			
			  </div>
                <!-- /.col-lg-6 -->
				
				
            </div>
            <!-- /.row -->
      </div>
    </div>
  </div>
   <div class="panel panel-default" id="twitter_data">
    <div class="panel-heading set-pad" role="tab" id="headingFour">
	
                    <h1  class="page-header text-center">
					 <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
					  Twitter Data
					</a>
					</h1>
                
                <!-- /.col-lg-12 -->
    
      
       
     
    </div>
    <div id="collapseFour" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingFour">
      <div class="panel-body">
         <div class="row">
			  	<?php
				if(!isset($_SESSION['twitter_followers'])){
					echo "<p align='center'>Twitter account not found or not provided.</p>";
				}
			?>
                <div class="col-lg-3 col-md-6">
                    
									<?php 
									if(isset($_SESSION['twitter_followers'])){
											echo '
											<div class="panel panel-primary">
										<div class="panel-heading">
											<div class="row">
												<div class="col-xs-3">
													<i class="fa fa-users fa-5x"></i>
												</div>
												<div class="col-xs-9 text-right">
													<div class="huge">
											'.$_SESSION['twitter_followers']."
														</div>
														<div>Followers</div>
													</div>
												</div>
											</div>
										
										</div>
											"; 
										}
										
									
										
									?>
						
                </div>
				
				 <div class="col-lg-3 col-md-6">
                    
									<?php
										if(isset($_SESSION['total_languages_tweets'])){
											echo '
											<div class="panel panel-green">
										<div class="panel-heading">
											<div class="row">
												<div class="col-xs-3">
													<i class="fa fa-twitter-square fa-5x"></i>
												</div>
												<div class="col-xs-9 text-right">
													<div class="huge">
											'.$_SESSION['total_languages_tweets']."
															</div>
															<div>Tweets about Languages</div>
														</div>
													</div>
												</div>
												
											</div>
											"; 
										}
										
									
										
									?>
					
                </div>
				
				 <div class="col-lg-3 col-md-6">
                    
									<?php
										if(isset($_SESSION['total_languages_retweets'])){
											echo '
											<div class="panel panel-yellow">
								<div class="panel-heading">
									<div class="row">
										<div class="col-xs-3">
											<i class="fa fa-retweet fa-5x"></i>
										</div>
										<div class="col-xs-9 text-right">
											<div class="huge">
											'.$_SESSION['total_languages_retweets']."
														</div>
										<div>  Retweets about Languages</div>
											</div>
										</div>
									</div>
									
								</div>
											"; 
										}
										
									
										
									?>
						
                </div>
				
					 <div class="col-lg-3 col-md-6">
                    
									<?php
										if(isset($_SESSION['total_languages_likes'])){
											echo '
								<div class="panel panel-red">
									<div class="panel-heading">
										<div class="row">
											<div class="col-xs-3">
												<i class="fa fa-heart fa-5x"></i>
											</div>
											<div class="col-xs-9 text-right">
												<div class="huge">
											'.$_SESSION['total_languages_likes']."
																</div>
												<div>Likes about Languages</div>
											</div>
										</div>
									</div>
									
								</div>
											"; 
										}
									
									
										
									?>
				
                </div>
               
            </div>
            <!-- /.row -->
			
			<div class="row">
			<div class="col-lg-12">
			
			
			
			</div>
			</div>
			
			<!-- Row to represent badges, answers and upvotes per language-->
				<div class="row">
				<div class="col-lg-6 ">
				 <?php
				 if(isset($_SESSION['tweets_by_language'])){
					echo '
						<div  class="set-pad">
						<div class="set-pad">
						<div class="panel panel-default">
								<div class="panel-heading">
									<i class="fa fa-bar-chart-o fa-fw"></i> Tweets by Language(%)
								</div>
								<div class="panel-body">
									<div id="twitter_graph"></div>
									<button class="btn btn-block btn-default" data-toggle="collapse" data-target="#twitter-collapse-table">View Details</button>
								';
					
					 if(isset($_SESSION['tweets_by_language'])){
				echo'
					
                    <div class="panel panel-default collapse" id="twitter-collapse-table">
                        <div class="panel-heading">
                           Tweets per Language
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-tweets">
                                    <thead>
									
                                        <tr>
                                            <th>#</th>
                                            <th>Language</th>
                                            <th>Tweets</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                        ';
									$table=$_SESSION['tweets_by_language'];
									$i=0;
									foreach($table as $k => $v){
										
										if($v){
											echo"
											<tr>
												<td>".($i+1)."</td>
												<td>".$k."</td>
												<td>".$v."</td>
											   
											</tr>
												";
												$i++;
										}
									}
									
				echo '					  
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
               
           
			';
				 }		
					
							
					echo'</div>
						<!-- /.panel-body -->
						</div>
						</div>
						</div>
					';
				}
				
				 ?>
				  </div>
                <!-- /.col-lg-4 -->
				 
				 
				 
				
				 <div class="col-lg-6 ">
				 <?php
				 
				  if(isset($_SESSION['retweets_by_language'])){
					echo '
						<div  class="set-pad">
						<div class="set-pad">
						<div class="panel panel-default">
								<div class="panel-heading">
									<i class="fa fa-bar-chart-o fa-fw"></i> Reweets by Language(%)
								</div>
								<div class="panel-body">
									<div id="twitter_graph_retweets"></div>
									<button class="btn btn-block btn-default" data-toggle="collapse" data-target="#twitter-collapse-table-retweets">View Details</button>
								';
					
					if(isset($_SESSION['retweets_by_language'])){
						echo'
					
						<div class="panel panel-default collapse" id="twitter-collapse-table-retweets">
							<div class="panel-heading">
							Retweets per Language
							</div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-retweets">
                                    <thead>
									
                                        <tr>
                                            <th>#</th>
                                            <th>Language</th>
                                            <th>Retweets</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                        ';
									$table=$_SESSION['retweets_by_language'];
									$i=0;
									foreach($table as $k => $v){
										
										if($v){
											echo"
											<tr>
												<td>".($i+1)."</td>
												<td>".$k."</td>
												<td>".$v."</td>
											   
											</tr>
												";
												$i++;
										}
									}
									
				echo '					  
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
              
				';
				}
					
							
					echo'</div>
						<!-- /.panel-body -->
						</div>
						</div>
						</div>
					';
				}
				 

			?>
			
			  </div>
                <!-- /.col-lg-6 -->
				
            </div>
            <!-- /.row -->
			
			<div class="row">
				
				<div class="col-lg-6 ">
				 <?php
				 
				 if(isset($_SESSION['likes_by_language'])){
					echo '
						<div  class="set-pad">
						<div class="set-pad">
						<div class="panel panel-default">
								<div class="panel-heading">
									<i class="fa fa-bar-chart-o fa-fw"></i> Likes by Language(%)
								</div>
								<div class="panel-body">
									<div id="twitter_graph_likes"></div>
									<button class="btn btn-block btn-default" data-toggle="collapse" data-target="#twitter-collapse-table-likes">View Details</button>
								';
					
					  if(isset($_SESSION['likes_by_language'])){
					echo'
					
                    <div class="panel panel-default collapse" id="twitter-collapse-table-likes">
                        <div class="panel-heading">
                           Tweet\'s Likes per Language
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-likes">
                                    <thead>
									
                                        <tr>
                                            <th>#</th>
                                            <th>Language</th>
                                            <th>Likes</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                        ';
									$table=$_SESSION['likes_by_language'];
									$i=0;
									foreach($table as $k => $v){
										//if($i==10)
											//break;
										if($v){
											echo"
											<tr>
												<td>".($i+1)."</td>
												<td>".$k."</td>
												<td>".$v."</td>
											   
											</tr>
												";
												$i++;
										}
									}
									
				echo '					  
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
              
				';
				}
					
							
					echo'</div>
						<!-- /.panel-body -->
						</div>
						</div>
						</div>
					';
				}
				 
				 
				
			?>
			
			  </div>
                <!-- /.col-lg-6 -->
				
				
            </div>
            <!-- /.row -->
      </div>
    </div>
  </div>
</div>
			
			</div>
			 <!-- /row -->
        </div>
        <!-- /#page-wrapper -->
    <!-- END Data-->
     


     <br>
	 <br>
	 <br>
	 <br>
	 <br>
          
     <!-- CONTACT SECTION END-->
    <div id="footer">
          &copy 2016 Expertise Analyzer | All Rights Reserved |  <a href="http://binarytheme.com" style="color: #fff" target="_blank">Design by : binarytheme.com</a>
    </div>
     <!-- FOOTER SECTION END-->
	 
	 <!-- BOOTSTRAP MIN JS -->
	<script src="assets/js/bootstrap.min.js" type="script"></script>
	<script src="assets/js/jquery-1.12.0.min.js" type="script"></script>
   
    <!--  Jquery Core Script -->
   <!-- <script src="assets/js/jquery-1.10.2.js"></script> -->
    <!--  Core Bootstrap Script -->
    <!-- <script src="assets/js/bootstrap.js"></script> -->
    <!--  Flexslider Scripts --> 
         <script src="assets/js/jquery.flexslider.js"></script>
     <!--  Scrolling Reveal Script -->
    <script src="assets/js/scrollReveal.js"></script>
    <!--  Scroll Scripts --> 
    <script src="assets/js/jquery.easing.min.js"></script>
    <!--  Custom Scripts --> 
         <script src="assets/js/custom.js"></script>
		 
		 <!-- jQuery -->
     <script src="bower_components/jquery/dist/jquery.min.js"></script> 

    <!-- Bootstrap Core JavaScript -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script> 

    <!-- Metis Menu Plugin JavaScript -->
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="bower_components/raphael/raphael-min.js"></script>
    <script src="bower_components/morrisjs/morris.min.js"></script>
    <!-- <script src="js/morris-data.js"></script> -->

	<!-- DataTables JavaScript -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
	
    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>
	
	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<!-- Script to work right datatable with id=dataTables-example -->
	<!-- For User Scores tables-->
	<script>
    $(document).ready(function() {
        $('#dataTables-social-activity').DataTable({
                responsive: true
        });
    });
    </script>
	
	<script>
    $(document).ready(function() {
        $('#dataTables-knowledge').DataTable({
                responsive: true
        });
    });
    </script>
	
	<!-- For stackoverflow tables-->
	  <script>
    $(document).ready(function() {
        $('#dataTables-badges').DataTable({
                responsive: true
        });
    });
    </script>
	  <script>
    $(document).ready(function() {
        $('#dataTables-answers').DataTable({
                responsive: true
        });
    });
    </script>
    <script>
    $(document).ready(function() {
        $('#dataTables-upvotes').DataTable({
                responsive: true
        });
    });
    </script>
	
	<!-- For twitter tables-->
	 <script>
    $(document).ready(function() {
        $('#dataTables-tweets').DataTable({
                responsive: true
        });
    });
    </script>
	 <script>
    $(document).ready(function() {
        $('#dataTables-retweets').DataTable({
                responsive: true
        });
    });
    </script>
	 <script>
    $(document).ready(function() {
        $('#dataTables-likes').DataTable({
                responsive: true
        });
    });
    </script>
	
	
	<?php
	//Print JS code to create graphs.
		//For User's Score
		if(isset($_SESSION['total_repos_percentages'])){
			echo $_SESSION['total_repos_percentages'];
		}
		//For GitHub
		if(isset($_SESSION['github_graph'])){
			echo $_SESSION['github_graph'];
		}
		if(isset($_SESSION['github_graph2'])){
			echo $_SESSION['github_graph2'];
		}
		if(isset($_SESSION['github_graph3'])){
			echo $_SESSION['github_graph3'];
		}
		//For Bitbucket.
		if(isset($_SESSION['bitbucket_commits_graph'])){
			echo $_SESSION['bitbucket_commits_graph'];
		}
		if(isset($_SESSION['bitbucket_percentages'])){
			echo $_SESSION['bitbucket_percentages'];
		}
		if(isset($_SESSION['bitbucket_repos-by-language-bar-chart'])){
			echo $_SESSION['bitbucket_repos-by-language-bar-chart'];
		}
		//For Stack Overflow
		if(isset($_SESSION['stack_graph1'])){
			echo $_SESSION['stack_graph1'];
		}
		if(isset($_SESSION['stack_graph2'])){
			echo $_SESSION['stack_graph2'];
		}
		//For Twitter
		if(isset($_SESSION['twitter_graph'])){
			echo $_SESSION['twitter_graph'];
		}
		if(isset($_SESSION['twitter_graph_retweets'])){
			echo $_SESSION['twitter_graph_retweets'];
		}
		if(isset($_SESSION['twitter_graph_likes'])){
			echo $_SESSION['twitter_graph_likes'];
		}
		
	?>	
		 
</body>
</html>

