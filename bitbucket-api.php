<?php
//Username of user.

function get_bitbucket_data($username,$languages){
//Change of max execution time because in some cases exceed the current time limit.
ini_set('max_execution_time', 800);

	$request = 'https://api.bitbucket.org/2.0/users/'.$username;
	$response  = file_get_contents($request);
	if(!$response){
		return null;
	}
	$jsonobj  = json_decode($response);
	
	//$commits_counter=array("2009"=>0,"2010"=>0,"2011"=>0,"2012"=>0,"2013"=>0,"2014"=>0,"2015"=>0,"2016"=>0);
	$languages_keys = array_keys($languages);
	
	//Initialize commits by language array.
	for($year=2009;$year<=2016;$year++){
		$year_str = strval($year);
		for($i=0;$i<count($languages_keys);$i++){
			$commits_counter[$year_str][$languages_keys[$i]] = 0;
		}
	}
	
	$page=1;
	$repositories_counter=0;
	do{
		//Url request.
		$request = 'https://api.bitbucket.org/2.0/repositories/'.$username.'?page='.$page;
		$response  = file_get_contents($request);
		$jsonobj  = json_decode($response);
		//Array with repos.
		$repos = $jsonobj->{'values'};
		$repositories_counter+=count($repos);
		for($i=0;$i<count($repos);$i++){
			$language = strtoupper($repos[$i]->{'language'});
			//Count the language of repo to the array.
			if(in_array($language,$languages_keys)){
				
				$languages[$language]++;
			}
			
			$commit_url=$repos[$i]->{'links'}->{'commits'}->{'href'};
			
			//Get commits for each repo.
			$page2=1;
			do{
				$commit_request=$commit_url."?page=".$page2;
				$response2  = file_get_contents($commit_request);
			

				$jsonobj2  = json_decode($response2);
				//Get the commits array.
				$commits = $jsonobj2->{'values'};
				//If no commits returned break.
				if(count($commits)==0)
					break;
				for($j=0;$j<count($commits);$j++){
					$author=$commits[$j]->{'author'}->{'user'}->{'username'};
					
					if((strcmp($author,$username)==0) && (in_array($language,$languages_keys)) ){
							$year = $commits[$j]->{"date"};
							$year= split('-',$year);
							$year=strval($year[0]);
							$commits_counter[$year][$language]++;
							
					}
				}
				$page2++;
			}while(count($commits)!=0);
		}
		//If array is empty break.
		if(count($repos)==0)
			break;
		
		
		$page++;
	}while(count($repos)!=0);
	
	$page=1;
	$followers_count=0;
	do{
		//Url request.
		$request = 'https://api.bitbucket.org/2.0/users/'.$username.'/followers?page='.$page;
		$response  = file_get_contents($request);
		$jsonobj  = json_decode($response);
		//Array with repos.
		$followers = $jsonobj->{'values'};
		$followers_count+=count($followers);
		//If array is empty break.
		if(count($followers)==0)
			break;
		
		
		$page++;
	}while(count($repos)!=0);
	
	//Skip zero values;
	//Keys for languages that has at least one commit in any year.
	$languages_exist_keys=array();
	$lek_counter=0;
	for($year=2009;$year<=2016;$year++){
		$year_str = strval($year);
		for($i=0;$i<count($languages_keys);$i++){
			if($commits_counter[$year_str][$languages_keys[$i]] != 0){
				$languages_exist_keys[$lek_counter]=$languages_keys[$i];
				$lek_counter++;
			}
		}
	}
	
	for($year=2009;$year<=2016;$year++){
		$year_str = strval($year);
		for($i=0;$i<count($languages_exist_keys);$i++){
			if($commits_counter[$year_str][$languages_exist_keys[$i]] != 0){
				$commits_counter_new[$year_str][$languages_exist_keys[$i]]=$commits_counter[$year_str][$languages_exist_keys[$i]];
			}
			else{
				$commits_counter_new[$year_str][$languages_exist_keys[$i]]=0;
			}
		}
	}
	$repositories_counter=0;
	foreach($languages as $k => $v){
		$repositories_counter+=$v;
	}
	
	
	return(array('followers' =>$followers_count, 'repos_by_language' => $languages, 'repositories_counter' => $repositories_counter,
				'commits' => $commits_counter_new));
}
?>