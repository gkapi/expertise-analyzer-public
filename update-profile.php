
<?php
session_start();
session_unset();
?>




<?php


	

//for cs ucy labs db
include('db-config.php');

$message;

	
	$github_username="";
	$stackoverflow_id="";
	$twitter_username="";
	$bitbucket_username="";
	
	
	//Check the usernames given by the user.
	if(isset($_POST['github_username'])){
		$github_username=$_POST['github_username'];
		
	}
	if(isset($_POST['stackoverflow_id'])){
		$stackoverflow_id=$_POST['stackoverflow_id'];
	}
	if(isset($_POST['twitter_username'])){
		$twitter_username=$_POST['twitter_username'];
	}
	
	if(isset($_POST['bitbucket_username'])){
		$bitbucket_username=$_POST['bitbucket_username'];
	}
	
	//****************************************************************************
	
	//***Check if user has any other account that is not given as input*******
	$conn = mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);
	if (!$conn) {
		echo "Unable to connect to DB: " . mysqli_error();
		exit;
	}
	$sql="SELECT * FROM expertanalyzer.user WHERE github_username='".$github_username."' OR
		stackoverflow_id='".$stackoverflow_id."' OR 
		twitter_username='".$twitter_username."' OR
		bitbucket_username='".$bitbucket_username."'";
	$rows = mysqli_query($conn,$sql);
	

	$row=mysqli_fetch_assoc($rows);
	
	//If user doesn't exis in DB and action= retrieve go to profile.
	if( !$row && !$create){
		$_SESSION['not_found']=true;
		header("Location: profile.php");
	}
	
	$temp_github_username=$row['github_username'];
	$temp_bitbucket_username=$row['bitbucket_username'];
	$temp_stackoverflow_id=$row['stackoverflow_id'];
	$temp_twitter_username=$row['twitter_username'];
	
	if(strcmp($temp_github_username,"")!=0){
		$github_username=$temp_github_username;
	}
	if(strcmp($temp_bitbucket_username,"")!=0){
		$bitbucket_username=$temp_bitbucket_username;
	}
	if(strcmp($temp_stackoverflow_id,"")!=0){
		$stackoverflow_id=$temp_stackoverflow_id;
	}
	if(strcmp($temp_twitter_username,"")!=0){
		$twitter_username=$temp_twitter_username;
	}
	////echo $twitter_username."-aaaa";
	mysqli_close($conn);
	
	//***END Check if user has any other account that is not given as input***
	
	
	//GET LANGUAGES FROM DB*****************************************************
	$conn = mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);
	
	if (!$conn) {
		echo "Unable to connect to DB: " . mysqli_error();
		exit;
	}
	$sql="SELECT language from expertanalyzer.languages";
	$rows = mysqli_query($conn,$sql);
	
	while($row=mysqli_fetch_assoc($rows)){
		//Eliminate last position of line that is the new line and we dont want to save it.
		$language=strtoupper($row['language']);
		$languages[$language]=0;
		
	}
	mysqli_close($conn);
	
	//END GET LANGUAGES FROM DB*************************************************
	
	
	
	//Github Data Fetch***********************************************************
	$github_account=false;
	$github_repos_exist=false;
	if($github_username!=""){
		$github_data=null;
		if(!$create){
			include("github-data-DB.php");
			$github_data=getDataDB($github_username);
			// echo"<br><br>";
			// var_dump($github_data);
			// echo"<br><br>";
		}
		else{
			include('github-api.php');
			$github_data=getData($github_username,$languages);
			// echo"<br><br>";
			// var_dump($github_data);
			// echo"<br><br>";
		}
		//Prepare GitHub data.
		include ("github-data-fetch.php");
	
	}
	
	//End Github Data Fetch*******************************************************
	
	//Bit Bucket Data Fetch*******************************************************
	$bitbucket_account=false;
	$createBitbucketRepos=false;
	if($bitbucket_username!=""){
		$bitbucket_data=null;
		if(!$create){
			include("bitbucket-data-DB.php");
			$bitbucket_data=get_bitbucket_dataDB($bitbucket_username);
			// echo"<br><br>";
			// var_dump($bitbucket_data);
			// echo"<br><br>";
		}
		else{
			include('bitbucket-api.php');
			$bitbucket_data=get_bitbucket_data($bitbucket_username,$languages);
			// echo"<br><br>";
			// var_dump($bitbucket_data);
			// echo"<br><br>";
		}
		//Prepare Bitbucket data.
		include("bitbucket-data-fetch.php");
	}
	
	
	//END Bit Bucket Data Fetch***************************************************
	
	
	
	//Stackoverflow Data fetch****************************************************
	
	$stackoverflow_account=false;
	if($stackoverflow_id!='' & ctype_digit($stackoverflow_id)){
		$stackoverflow_id=intval($stackoverflow_id);
		
		if(!$create){
			include("stackoverflow-data-DB.php");
			$stackoverflow_data=getDataStackOverflowDB($stackoverflow_id);
			//echo"<br><br>";
			//var_dump($stackoverflow_data);
			//echo"<br><br>";
		}
		else{
			include('stackoverflow-api.php');
			$stackoverflow_data=getDataStackOverflow($stackoverflow_id,$languages);
			//echo"<br><br>";
			//var_dump($stackoverflow_data);
			//echo"<br><br>";
		}
		
		//Prepare Stack Overflow data.
		include("stackoverflow-data-fetch.php");
		
		
	}
	
	//END Stackoverflow Data fetch************************************************
	
	
	
	
	
	//Twitter Data fetch**********************************************************
	
	$twitter_account=false;
	if($twitter_username!=""){
		
		if(!$create){
			include('twitter-data-DB.php');
			$twitter_data=getTwitterDataDB($twitter_username);
			//echo"<br><br>";
			//var_dump($twitter_data);
			//echo"<br><br>";
			
		}
		else{
			include('twitter-api.php');
			$twitter_data=getTwitterData($twitter_username, $languages);
			//echo"<br><br>";
			//var_dump($twitter_data);
			//echo"<br><br>";
		}
		
		//Prepare Twitter data.
		include("twitter-data-fetch.php");
		
	}
	
	//END Twitter Data fetch******************************************************
	
	
	
	//Get Stackoverflow max answers by language from DB*******************
	if($stackoverflow_account){
		$max_answers=null;
		
		$conn = mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);
		
		if (!$conn) {
			echo "Unable to connect to DB: " . mysqli_error();
			exit;
		}
		
		$sql="SELECT language,max_answers from expertanalyzer.stackoverflow_max_answers";
		$rows = mysqli_query($conn,$sql);
		
		while($row=mysqli_fetch_assoc($rows)){
			$language=strtoupper($row['language']);
			$max_answers[$language]=$row['max_answers'];
			
		}
		mysqli_close($conn);
		
	}
	//END Read stack overflow max answers by language from DB***************
	
	
	
	//********************************************DataBase*******************************************************
	//Insert user's data in DB if the action is create.
	
	include("insert-in-DB.php");
	//****************************************END DataBase*******************************************************
	
	
	//**********************************************USER SCORE***************************************************
	
	//********************************************Most experienced in.***********************************
	//Values that don't exist are assumed to be zero.
	$experience;
	if($github_account){
		foreach($repos_by_language as $k => $v){
			$experience[$k]=$v*0.4;
		}
	}
	if($bitbucket_account){
		foreach($bitbucket_repos_by_language as $k => $v){
			if(isset($experience[$k])){
				$experience[$k]+=$v*0.4;
			}
			else{
				$experience[$k]=$v*0.4;
			}
				
			
		}
	}
	if($stackoverflow_account){
		foreach($answers_about_languages as $k => $v){
			if(isset($experience[$k])){
				$experience[$k]+=$v*0.5*0.4;
			}
			else{
				$experience[$k]=$v*0.5*0.4;
			}
		}
		foreach($answers_upvotes_about_languages as $k => $v){
			if(isset($experience[$k])){
				$experience[$k]+=$v*0.5*0.4;
			}
			else{
				$experience[$k]=$v*0.5*0.4;
			}
		}
	}
	if($twitter_account){
		foreach($tweets_by_language as $k => $v){
			if(isset($experience[$k])){
				$experience[$k]+=$v*0.4*0.3;
			}
			else{
				$experience[$k]=$v*0.4*0.3;
			}
		}
		foreach($retweets_by_language as $k => $v){
			if(isset($experience[$k])){
				$experience[$k]+=$v*0.3*0.3;
			}
			else{
				$experience[$k]=$v*0.3*0.3;
			}
		}
		foreach($likes_by_language as $k => $v){
			if(isset($experience[$k])){
				$experience[$k]+=$v*0.3*0.3;
			}
			else{
				$experience[$k]=$v*0.3*0.3;
			}
		}
	}
	$most_experienced_value=0;
	$most_experienced_language="";
	foreach($experience as $k => $v){
		if($v>$most_experienced_value){
			$most_experienced_value=$v;
			$most_experienced_language=$k;
		}
	}
	$_SESSION['most_experienced_in']=$most_experienced_language;
	
	//********************************END Most experienced in*********************************************
	
	
	//**********************************Joined system since***********************************************
	if(isset($db_last_update)){
		$joined=$db_last_update;
	}
	else{
		$joined=$now;
	}
	$_SESSION["joined"]=$joined;
	//**********************************END Joined system since*******************************************
	
	
	//******************************** Total Commits ************************************************
	$total_commits=0;
	//GitHub commits
	foreach($commits_by_year as $k => $v){
		foreach($v as $kk => $vv){
			$total_commits+=$vv;
		}
	}
	//Bitbucket commits
	foreach($bitbucket_commits_by_year as $k => $v){
		foreach($v as $kk => $vv){
			$total_commits+=$vv;
		}
	}
	$_SESSION['total_commits']=$total_commits;
	
	//********************************END Total Commits *********************************************
	
	
	//include('max-values.php');
	//Get Max values
	$conn = mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);
	
	
	if (!$conn) {
		echo "Unable to connect to DB: " . mysqli_error();
		exit;
	}
	//Github max followers.
	$sql="SELECT * FROM expertanalyzer.max_values";
	$rows=mysqli_query($conn,$sql);
	while($row=mysqli_fetch_assoc($rows)){
		$name=$row['name'];
		$value=$row['value'];
		define($name,$value);
	}
	mysqli_close($conn);
	
	
	//********Popularity calculation, includes: Github Followers and Total Repos, Stackoverflow Reputation.*******
	//Total repositories, Github + Bit Bucket.
	$total_repos=$github_repos_counter + $bitbucket_repos;
	$_SESSION['total_repos']=$total_repos;
	
	//Total languages' percentages.
	if($createBitbucketRepos || $github_repos_exist){
		//Both accounts exist.
		if($createBitbucketRepos && $github_repos_exist){
			// for($i=0;$i<count($percentages);$i++){
				// $total_repos_percentages[$bitbucket_keys[$i]]=$percentages[$bitbucket_keys[$i]]+$bitbucket_percentages[$bitbucket_keys[$i]];
			// }
			for($i=0;$i<count($percentages);$i++){
				$total_repos_percentages[$keys[$i]]=$percentages[$keys[$i]]/2;
			}
			for($i=0;$i<count($bitbucket_percentages);$i++){
				if(isset($total_repos_percentages[$bitbucket_keys[$i]])){
					$total_repos_percentages[$bitbucket_keys[$i]]+=$bitbucket_percentages[$bitbucket_keys[$i]]/2;
				}
				else
					$total_repos_percentages[$bitbucket_keys[$i]]=$bitbucket_percentages[$bitbucket_keys[$i]]/2;
			}
		}
		//If only github exist.
		else if($github_repos_exist){
			for($i=0;$i<count($percentages);$i++){
				$total_repos_percentages[$keys[$i]]=$percentages[$keys[$i]];
			}
		}
		//If only Bit bucket exist.
		else{
			for($i=0;$i<count($bitbucket_percentages);$i++){
				$total_repos_percentages[$bitbucket_keys[$i]]=$bitbucket_percentages[$bitbucket_keys[$i]];
			}
		}
		$total_repos_keys = array_keys($total_repos_percentages);
		$_SESSION['total_repos_percentages']="<script>$(function() {
			Morris.Donut({
				element: 'total_repos_percentages',
				data: [";
		for($i=0;$i<count($total_repos_percentages);$i++){
			if($total_repos_percentages[$total_repos_keys[$i]]==0){
				continue;
			}
			$_SESSION['total_repos_percentages'].=" { label: '".$total_repos_keys[$i]."', value: ".number_format($total_repos_percentages[$total_repos_keys[$i]],2)."  }";
			
			if($i<count($total_repos_percentages)-1){
				$_SESSION['total_repos_percentages'].=", ";
			}
			
		}
		$_SESSION['total_repos_percentages'].="
					],
					 formatter: function (x) { return x + '%'},
				resize: true
			});
			});
			</script>";
	}
	
	
	//If an account is not given, it has value 1.
	//Number of valid popularity measures. 
	$popularity_numbers=0;
	
	$git_popularity=0;
	if(isset($github_followers)){
		$git_popularity = ($github_followers/GITHUB_FOLLOWERS);
		$popularity_numbers++;
	}
	
	//BitBucket followers is found via email to BitBucket support.
	$bit_popularity=0;
	if(isset($bitbucket_followers)){
		$bit_popularity = ($bitbucket_followers/BITBUCKET_FOLLOWERS);
		$popularity_numbers++;
	}
	
	$stack_popularity=0;
	if(isset($reputation)){
		$stack_popularity = ($reputation/REPUTATION);
		$popularity_numbers++;
	}
	//Consider only valid accounts given.
	if($popularity_numbers>0){
		$popularity_percentage = 1 / $popularity_numbers;
	
	
		$score_popularity = ($git_popularity*$popularity_percentage)+
							($stack_popularity*$popularity_percentage)+
							($bit_popularity*$popularity_percentage);
		
		
		$_SESSION['score_popularity']=number_format(($score_popularity*100),2);
	}
	
		//*******************System Popularity.**************************************
	// System Popularity calculation, includes: Github Followers, BitBucket followers, Stackoverflow Reputation
	// and Twitter followers, all from the System Expertise Analyzer.
	$conn = mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);
	
	
	if (!$conn) {
		echo "Unable to connect to DB: " . mysqli_error();
		exit;
	}
	//Github max followers.
	$sql="SELECT max(followers) as followers FROM expertanalyzer.github_user";
	$rows=mysqli_query($conn,$sql);
	if(mysqli_num_rows($rows)>0){
		$row=mysqli_fetch_assoc($rows);
		$system_github_max_followers=$row['followers'];
	}
	//Bit Bucket max followers.
	$sql="SELECT max(followers) as followers FROM expertanalyzer.bitbucket_user";
	$rows=mysqli_query($conn,$sql);
	if(mysqli_num_rows($rows)>0){
		$row=mysqli_fetch_assoc($rows);
		$system_bitbucket_max_followers=$row['followers'];
	}
	//Stack Overflow max reputation.
	$sql="SELECT max(reputation) as reputation FROM expertanalyzer.stackoverflow_user";
	$rows=mysqli_query($conn,$sql);
	if(mysqli_num_rows($rows)>0){
		$row=mysqli_fetch_assoc($rows);
		$system_stackoverflow_max_reputation=$row['reputation'];
	}
	//Twitter max followers.
	$sql="SELECT max(followers) as followers FROM expertanalyzer.twitter_user";
	$rows=mysqli_query($conn,$sql);
	if(mysqli_num_rows($rows)>0){
		$row=mysqli_fetch_assoc($rows);
		$system_twitter_max_followers=$row['followers'];
	}
	mysqli_close($conn);
	//echo"dame<br>";
	//echo $system_github_max_followers."-".$system_bitbucket_max_followers."-".$system_stackoverflow_max_reputation."-".$system_twitter_max_followers;
	//echo "<br>";
	//echo $github_followers."-".$bitbucket_followers."-".$reputation."-".$twitter_followers;
	//echo "<br>";
	//If an account is not given, it has value 1.
	//Number of valid popularity measures. 
	$popularity_numbers=0;
	
	$git_popularity=0;
	if(isset($github_followers) && isset($system_github_max_followers)){
		$git_popularity = ($github_followers/$system_github_max_followers);
		$popularity_numbers++;
	}
	
	//BitBucket followers is found via email to BitBucket support.
	$bit_popularity=0;
	if(isset($bitbucket_followers) && isset($system_bitbucket_max_followers)){
		$bit_popularity = ($bitbucket_followers/$system_bitbucket_max_followers);
		$popularity_numbers++;
	}
	
	$stack_popularity=0;
	if(isset($reputation) && isset($system_stackoverflow_max_reputation)){
		$stack_popularity = ($reputation/$system_stackoverflow_max_reputation);
		$popularity_numbers++;
	}
	$twitter_popularity=0;
	if(isset($twitter_followers) && isset($system_twitter_max_followers)){
		$twitter_popularity=($twitter_followers/$system_twitter_max_followers);
		$popularity_numbers++;
	}

	if($popularity_numbers>0){
		$popularity_percentage = 1 / $popularity_numbers;
	
	
		$system_popularity = ($git_popularity*$popularity_percentage)+
							($stack_popularity*$popularity_percentage)+
							($bit_popularity*$popularity_percentage)+
							($twitter_popularity*$popularity_percentage);
	
		$_SESSION['system_popularity']=number_format(($system_popularity*100),2);
			//var_dump($system_popularity);
	}
	
	//*******************Knowledge for each language.****************************
	
	//Derived from stack overflow answers.
	//GET Max upvotes FROM DB
	$stack_max_upvotes = null;
	$conn = mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);
	
	if (!$conn) {
		echo "Unable to connect to DB: " . mysqli_error();
		exit;
	}
	$sql="SELECT language,max_upvotes from expertanalyzer.stackoverflow_max_upvotes";
	$rows = mysqli_query($conn,$sql);
	
	while($row=mysqli_fetch_assoc($rows)){
		//Eliminate last position of line that is the new line and we dont want to save it.
		$language=strtoupper($row['language']);
		$stack_max_upvotes[$language]=$row['max_upvotes'];
		
	}
	mysqli_close($conn);
	
	//END GET Max upvotes FROM DB
	
	$badges_about_languages;
	
	
	arsort($answers_about_languages,true);
	$aal_keys = array_keys($answers_about_languages);
	
	$score_knowledge_by_language;
	
	
	for($i=0;$i<count($aal_keys);$i++){
		
		if($answers_about_languages[$aal_keys[$i]]==0)
			break;
		//Get the type of badge if the user has one.
		if(strcmp($badges_about_languages[$aal_keys[$i]],"gold")==0)
			$badge =0.30;
		else
		if(strcmp($badges_about_languages[$aal_keys[$i]],"silver")==0)
			$badge=0.20;
		else
		if(strcmp($badges_about_languages[$aal_keys[$i]],"gold")==0)
			$badge=0.10;
		else
			$badge=0;
		
		$answers=$answers_about_languages[$aal_keys[$i]];
		
		$score_knowledge_by_language[$aal_keys[$i]]=($answers/$max_answers[$aal_keys[$i]])*0.5 +
													($answers_upvotes_about_languages[$aal_keys[$i]]/
													($stack_max_upvotes[$aal_keys[$i]]))*0.2+
													$badge;
		//echo $aal_keys[$i]." score = ".$score_knowledge_by_language[$aal_keys[$i]]."<br>";											
	}
	arsort($score_knowledge_by_language,true);
	$_SESSION['score_knowledge_by_language']=$score_knowledge_by_language;
	
	
	
	//***************************************System expertise************************************************
	//Three types of users: Beginner, Intermediate, Veteran
	$conn = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);
	if($conn->connect_error){
		die("Connection failed: ".mysql_connect_error());
	}
	
	//Max account duration from db.
	$sql="SELECT max(account_days) as account_days FROM expertanalyzer.github_user";
	$rows=mysqli_query($conn,$sql);
	if(mysqli_num_rows($rows)>0){
		$row=mysqli_fetch_assoc($rows);
		$system_github_max_account = $row['account_days'];
	}
	//Max github repos from db.
	$sql="CREATE TEMPORARY TABLE repos (
		username varchar(25),
        projects int(4)
                    );";
	mysqli_query($conn,$sql);
	$sql="INSERT INTO repos SELECT username,sum(projects) FROM expertanalyzer.github GROUP BY username;";
	mysqli_query($conn,$sql);
	$sql="SELECT max(projects) as projects FROM repos; "; 
	$rows=mysqli_query($conn,$sql);
	$sql="DROP TABLE repos;";
	mysqli_query($conn,$sql);
	if(mysqli_num_rows($rows)>0){
		$row=mysqli_fetch_assoc($rows);
		$system_github_max_repos = $row['projects'];
	}

	//Max bitbucket repos from db.
	$sql="CREATE TEMPORARY TABLE repos (
		username varchar(25),
        projects int(4)
                    );";
	mysqli_query($conn,$sql);
	$sql="INSERT INTO repos SELECT username,sum(projects) FROM expertanalyzer.bitbucket GROUP BY username;";
	mysqli_query($conn,$sql);
	$sql="SELECT max(projects) as projects FROM repos; "; 
	$rows=mysqli_query($conn,$sql);
	$sql="DROP TABLE repos;";
	mysqli_query($conn,$sql);
	
	if(mysqli_num_rows($rows)>0){
		$row=mysqli_fetch_assoc($rows);
		$system_bitbucket_max_repos = $row['projects'];
	}
	
	//Max gold badges from db.
	$sql="SELECT max(gold_badges) as gold_badges FROM expertanalyzer.stackoverflow_user";
	$rows=mysqli_query($conn,$sql);
	if(mysqli_num_rows($rows)>0){
		$row=mysqli_fetch_assoc($rows);
		$system_stack_max_gold = $row['gold_badges'];
	}
	//Max silver badges from db.
	$sql="SELECT max(silver_badges) as silver_badges FROM expertanalyzer.stackoverflow_user";
	$rows=mysqli_query($conn,$sql);
	if(mysqli_num_rows($rows)>0){
		$row=mysqli_fetch_assoc($rows);
		$system_stack_max_silver = $row['silver_badges'];
	}
	//Max bronze badges from db.
	$sql="SELECT max(bronze_badges) as bronze_badges FROM expertanalyzer.stackoverflow_user";
	$rows=mysqli_query($conn,$sql);
	if(mysqli_num_rows($rows)>0){
		$row=mysqli_fetch_assoc($rows);
		$system_stack_max_bronze = $row['bronze_badges'];
	}
	
	//Max tweets from db.
	$sql="CREATE TEMPORARY TABLE tweets (username varchar(25), tweets int(8));";
	mysqli_query($conn,$sql);
	$sql="INSERT INTO tweets SELECT username,sum(tweets) FROM expertanalyzer.twitter GROUP BY username;";
	mysqli_query($conn,$sql);
	$sql="SELECT max(tweets) as tweets FROM tweets;"; 
	$rows=mysqli_query($conn,$sql);
	$sql="DROP TABLE tweets;";
	mysqli_query($conn,$sql);
	if(mysqli_num_rows($rows)>0){
		$row=mysqli_fetch_assoc($rows);
		$system_stack_max_tweets = $row['tweets'];
	}
	//Max retweets from db.
	$sql="CREATE TEMPORARY TABLE retweets (username varchar(25), retweets int(8));";
	mysqli_query($conn,$sql);
	$sql="INSERT INTO retweets SELECT username,sum(retweets) FROM expertanalyzer.twitter GROUP BY username;";
	mysqli_query($conn,$sql);
	$sql="SELECT max(retweets) as retweets FROM retweets;"; 
	$rows=mysqli_query($conn,$sql);
	$sql="DROP TABLE retweets;";
	mysqli_query($conn,$sql);
	if(mysqli_num_rows($rows)>0){
		$row=mysqli_fetch_assoc($rows);
		$system_stack_max_retweets = $row['retweets'];
	}
	
	//Max tweets' likes from db.
	$sql="CREATE TEMPORARY TABLE likes (username varchar(25), likes int(8));";
	mysqli_query($conn,$sql);
	$sql="INSERT INTO likes SELECT username,sum(likes) FROM expertanalyzer.twitter GROUP BY username;";
	mysqli_query($conn,$sql);
	$sql="SELECT max(likes) as likes FROM likes;"; 
	$rows=mysqli_query($conn,$sql);
	$sql="DROP TABLE likes;";
	mysqli_query($conn,$sql);
	if(mysqli_num_rows($rows)>0){
		$row=mysqli_fetch_assoc($rows);
		$system_stack_max_likes = $row['likes'];
	}
	
	//Close db connection.
	mysqli_close($conn);
	
	
	$expertise_numbers=0;
	// Expertise = User's value/System's max value
	if(isset($github_repos_counter)){
		$expertise_github_repos=$github_repos_counter/$system_github_max_repos;
		$expertise_numbers++;
	}
	else{
		$expertise_github_repos=0;
	}
		
	if(isset($account_duration)){
		$expertise_github_account=$account_duration/$system_github_max_account;
		$expertise_numbers++;
	}
	else{
		$expertise_github_account=0;
	}
		
	if(isset($bitbucket_repos)){
		$expertise_bitbucket_repos=$bitbucket_repos/$system_bitbucket_max_repos;
		$expertise_numbers++;
	}
	else{
		$expertise_bitbucket_repos=0;
	}
		
	if(isset($gold_badges)){
		$expertise_gold_badges=$gold_badges/$system_stack_max_gold;
		$expertise_numbers++;
	}
	else{
		$expertise_gold_badges=0;
	}
		
	if(isset($silver_badges)){
		$expertise_silver_badges=$silver_badges/$system_stack_max_silver;
		$expertise_numbers++;
	}
	else{
		$expertise_silver_badges=0;
	}
		
	if(isset($bronze_badges)){
		$expertise_bronze_badges=$bronze_badges/$system_stack_max_bronze;
		$expertise_numbers++;
	}
	else{
		$expertise_bronze_badges=0;
	}
	
	//Calculate expertise
	if($expertise_numbers>0){
		$expertise_percentage=1/$expertise_numbers;
		$expertise=($expertise_github_repos)+
					($expertise_github_account)+
					($expertise_bitbucket_repos)+
					($expertise_gold_badges)+
					($expertise_silver_badges)+
					($expertise_bronze_badges);
		$expertise = $expertise * $expertise_percentage;
	}
	
	$_SESSION['expertise']=number_format(($expertise*100),2);
	if($_SESSION['expertise']<25)
		$_SESSION['expertise_type']="beginner";
	else
	if($_SESSION['expertise']<75)
		$_SESSION['expertise_type']="intermediate";
	else
		$_SESSION['expertise_type']="veteran";
	
	//****************User social activity(only from System)*********************************
	//Using stack overflow answers, upvotes , twitter tweets,likes and retweets.
	
	//Initialize associative array with all languages.
	foreach($languages as $k => $v){
		$system_social_activity[$k]=0;
	}
	
	//DB connection
	$conn = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);
	if($conn->connect_error){
		die("Connection failed: ".mysql_connect_error());
	}
	if($twitter_account || $stackoverflow_account)
	foreach($system_social_activity as $k => $v){
		$count=0;
		//If user has not any tweets and has not any answers in language $k continue.
		if($tweets_by_language[$k]==0 && $answers_about_languages[$k]==0)
			continue;
		$temp_answers=0;
		$temp_likes=0;
		$temp_retweets=0;
		$temp_tweets=0;
		$temp_upvotes=0;
		//Has twitter account
		if($twitter_account){
			//Get max tweets by language
			$sql="SELECT language, max(tweets) as max_tweets
					FROM expertanalyzer.twitter
					where language ='".strval($k)."'";
			$rows=mysqli_query($conn,$sql);
			if(mysqli_num_rows($rows)>0){
				$row=mysqli_fetch_assoc($rows);
				$system_max_language_tweets = $row['max_tweets'];
				
				if($system_max_language_tweets==0)
					$temp_tweets=0;
				else{
					$temp_tweets = $tweets_by_language[$k]/$system_max_language_tweets;
					//Plus one value.
					$count++;
				}
			}
			else
				$temp_tweets=0;
			//Get max retweets by language
			$sql="SELECT language, max(retweets) as max_retweets
					FROM expertanalyzer.twitter
					where language ='".strval($k)."'";
			$rows=mysqli_query($conn,$sql);
			if(mysqli_num_rows($rows)>0){
				$row=mysqli_fetch_assoc($rows);
				$system_max_language_retweets = $row['max_retweets'];
				
				if($system_max_language_retweets==0)
					$temp_retweets=0;
				else{
					$temp_retweets = $retweets_by_language[$k]/$system_max_language_retweets;
					//Plus one value.
					$count++;
				}
			}
			else
				$temp_retweets=0;
			
			//Get max likes by language
			$sql="SELECT language, max(likes) as max_likes
					FROM expertanalyzer.twitter
					where language ='".strval($k)."'";
			$rows=mysqli_query($conn,$sql);
			if(mysqli_num_rows($rows)>0){
				$row=mysqli_fetch_assoc($rows);
				$system_max_language_likes = $row['max_likes'];
				
				if($system_max_language_likes==0)
					$temp_likes=0;
				else{
					$temp_likes = $likes_by_language[$k]/$system_max_language_likes;
					//Plus one value.
					$count++;
				}
			}
			else
				$temp_likes=0;
			
		}
		
		//Has stack overflow account
		if($stackoverflow_account){
			//Get max answers by language
			$sql="SELECT language, max(answers) as max_answers
					FROM expertanalyzer.stackoverflow
					where language ='".strval($k)."'";
			$rows=mysqli_query($conn,$sql);
			if(mysqli_num_rows($rows)>0){
				$row=mysqli_fetch_assoc($rows);
				$system_max_language_answers = $row['max_answers'];
				
				if($system_max_language_answers==0)
					$temp_answers=0;
				else{
					$temp_answers = $answers_about_languages[$k]/$system_max_language_answers;
					//Plus one value.
					$count++;
				}
			}
			else
				$temp_answers=0;
			
			//Get max answers' upvotes by language
			$sql="SELECT language, max(upvotes) as max_upvotes
					FROM expertanalyzer.stackoverflow
					where language ='".strval($k)."'";
			$rows=mysqli_query($conn,$sql);
			if(mysqli_num_rows($rows)>0){
				$row=mysqli_fetch_assoc($rows);
				$system_max_language_upvotes = $row['max_upvotes'];
				
				if($system_max_language_upvotes==0)
					$temp_upvotes=0;
				else{
					$temp_upvotes = $answers_upvotes_about_languages[$k]/$system_max_language_upvotes;
					//Plus one value.
					$count++;
				}
			}
			else
				$temp_upvotes=0;
		}
		if($count!=0)
			$system_social_activity[$k]=number_format((($temp_answers+$temp_likes+$temp_retweets+$temp_tweets+$temp_upvotes)/$count*100),2);
		
	}
	arsort($system_social_activity, true);
	//If some values are greater than zero save table to session.
	foreach($system_social_activity as $k => $v)
		if($v>0){
			$_SESSION['system_social_activity']=$system_social_activity;
			break;
		}
	
	
	//Close db connection.
	
	mysqli_close($conn);
	
	//**********************************END USER SCORE***************************************
	
	//Insert user's scores in DB
	include("insert-scores-in-DB.php");
	
	
	
	
	?>

	<?php
	header("Location: profile.php");
	
?>

 