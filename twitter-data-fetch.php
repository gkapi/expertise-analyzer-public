<?php

if($twitter_data){
	$_SESSION['twitter_username']=$twitter_username;
	$twitter_account=true;
	$twitter_followers=$twitter_data['followers'];
	$tweets_by_language=$twitter_data['tweets_by_language'];
	$retweets_by_language=$twitter_data['retweets_by_language'];
	$likes_by_language=$twitter_data['likes_by_language'];
	
	if($tweets_by_language){
		//Calculate totals.
		$total_languages_tweets=0;
		foreach($tweets_by_language as $k => $v){
			$total_languages_tweets+=$v;
		}
		$total_languages_retweets=0;
		foreach($retweets_by_language as $k => $v){
			$total_languages_retweets+=$v;
		}
		$total_languages_likes=0;
		foreach($likes_by_language as $k => $v){
			$total_languages_likes+=$v;
		}
		
		//Calculate percentages for each project's language of total repos.
		arsort($tweets_by_language,true);
		$tweets_counter=0;
		foreach($tweets_by_language as $k => $v){
				$tweets_counter+=$v;
		}
		$tweets_percentages = new ArrayObject($tweets_by_language);
		
		$tweets_keys = array_keys($tweets_by_language);
		//$percentages=$bitbucket_repos_by_language->getArrayCopy();
		$low_percentages=0;
		$others_pointer=0;
		if($tweets_counter!=0){
			for($i=0;$i<count($tweets_percentages);$i++){
				$tweets_percentages[$tweets_keys[$i]]=number_format($tweets_percentages[$tweets_keys[$i]]/$tweets_counter*100, 2, '.', '');
				//Languages with percentages < 2%.
				if($tweets_percentages[$tweets_keys[$i]]<2){
					$low_percentages+=$tweets_percentages[$tweets_keys[$i]];
					if($others_pointer==0){
						$others_pointer=$i;
					}
				}
			}
		}
		
		
			$_SESSION['twitter_graph']="<script>$(function() {
			Morris.Donut({
				element: 'twitter_graph',
				data: [";
				for($i=0;$i<count($tweets_percentages);$i++){
					if($others_pointer>0 && $i==$others_pointer){
						$_SESSION['twitter_graph'].=" { label: 'Others', value: ".$low_percentages."  }";
						break;
					}
					else{
						if($tweets_percentages[$tweets_keys[$i]]==0){
							continue;
						}
						$_SESSION['twitter_graph'].=" { label: '".$tweets_keys[$i]."', value: ".$tweets_percentages[$tweets_keys[$i]]."  }";
						
						if($i<count($tweets_percentages)-1){
							$_SESSION['twitter_graph'].=", ";
						}
					}
					
				}
				$_SESSION['twitter_graph'].="
					],
					 formatter: function (x) { return x + '%'},
				resize: true
			});
			});
			</script>";
			
			
		
		//Calculate percentages for each project's language of total repos.
		arsort($retweets_by_language,true);
		$retweets_counter=0;
		foreach($retweets_by_language as $k => $v){
				$retweets_counter+=$v;
		}
		$retweets_percentages = new ArrayObject($retweets_by_language);
		
		$retweets_keys = array_keys($retweets_by_language);
		//$percentages=$bitbucket_repos_by_language->getArrayCopy();
		$low_percentages=0;
		$others_pointer=0;
		if($retweets_counter!=0){
			for($i=0;$i<count($retweets_percentages);$i++){
				$retweets_percentages[$retweets_keys[$i]]=number_format($retweets_percentages[$retweets_keys[$i]]/$retweets_counter*100, 2, '.', '');
				//Languages with percentages < 2%.
				if($retweets_percentages[$retweets_keys[$i]]<2){
					$low_percentages+=$retweets_percentages[$retweets_keys[$i]];
					if($others_pointer==0){
						$others_pointer=$i;
					}
				}
			}
		}
		
		
			$_SESSION['twitter_graph_retweets']="<script>$(function() {
			Morris.Donut({
				element: 'twitter_graph_retweets',
				data: [";
				for($i=0;$i<count($retweets_percentages);$i++){
					if($others_pointer>0 && $i==$others_pointer){
						$_SESSION['twitter_graph_retweets'].=" { label: 'Others', value: ".$low_percentages."  }";
						break;
					}
					else{
						if($retweets_percentages[$retweets_keys[$i]]==0){
							continue;
						}
						$_SESSION['twitter_graph_retweets'].=" { label: '".$retweets_keys[$i]."', value: ".$retweets_percentages[$retweets_keys[$i]]."  }";
						
						if($i<count($retweets_percentages)-1){
							$_SESSION['twitter_graph_retweets'].=", ";
						}
					}
					
				}
				$_SESSION['twitter_graph_retweets'].="
					],
					 formatter: function (x) { return x + '%'},
				resize: true
			});
			});
			</script>";
			
			
			
		//Calculate percentages for each project's language of total repos.
		arsort($likes_by_language,true);
		$likes_counter=0;
		foreach($likes_by_language as $k => $v){
				$likes_counter+=$v;
		}
		$likes_percentages = new ArrayObject($likes_by_language);
		
		$likes_keys = array_keys($likes_by_language);
		//$percentages=$bitbucket_repos_by_language->getArrayCopy();
		$low_percentages=0;
		$others_pointer=0;
		if($likes_counter!=0){
			for($i=0;$i<count($likes_percentages);$i++){
				$likes_percentages[$likes_keys[$i]]=number_format($likes_percentages[$likes_keys[$i]]/$likes_counter*100, 2, '.', '');
				//Languages with percentages < 2%.
				if($likes_percentages[$likes_keys[$i]]<2){
					$low_percentages+=$likes_percentages[$likes_keys[$i]];
					if($others_pointer==0){
						$others_pointer=$i;
					}
				}
			}
		}
		
		
			$_SESSION['twitter_graph_likes']="<script>$(function() {
			Morris.Donut({
				element: 'twitter_graph_likes',
				data: [";
				for($i=0;$i<count($likes_percentages);$i++){
					if($others_pointer>0 && $i==$others_pointer){
						$_SESSION['twitter_graph_likes'].=" { label: 'Others', value: ".$low_percentages."  }";
						break;
					}
					else{
						if($likes_percentages[$likes_keys[$i]]==0){
							continue;
						}
						$_SESSION['twitter_graph_likes'].=" { label: '".$likes_keys[$i]."', value: ".$likes_percentages[$likes_keys[$i]]."  }";
						
						if($i<count($likes_percentages)-1){
							$_SESSION['twitter_graph_likes'].=", ";
						}
					}
					
				}
				$_SESSION['twitter_graph_likes'].="
					],
					 formatter: function (x) { return x + '%'},
				resize: true
			});
			});
			</script>";
		
		$_SESSION['total_languages_tweets']=$total_languages_tweets;
		$_SESSION['total_languages_retweets']=$total_languages_retweets;
		$_SESSION['total_languages_likes']=$total_languages_likes;
		
		foreach($tweets_by_language as $k => $v)
			if($v>0){
				$_SESSION['tweets_by_language']=$tweets_by_language;
				break;
			}
		foreach($retweets_by_language as $k => $v)
			if($v>0){
				$_SESSION['retweets_by_language']=$retweets_by_language;
				break;
			}
		foreach($likes_by_language as $k => $v)
			if($v>0){
				$_SESSION['likes_by_language']=$likes_by_language;
				break;
			}
		
	}
	//Assign values to session.
	$_SESSION['twitter_followers']=$twitter_followers;
	
}

?>