<?php

//Get Twitter data from DB
function getTwitterDataDB($twitter_username){
	$data=null;
	$conn = mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);
	
	//Retrieve data from table twitter
	$sql="SELECT * FROM expertanalyzer.twitter WHERE username='".$twitter_username."'";
	$rows=mysqli_query($conn,$sql);
	
	$language;
	$tweets;
	$retweets;
	$likes;
	$tweets_by_language;
	$retweets_by_language;
	$likes_by_language;
	while($row=mysqli_fetch_assoc($rows)){
		$language=$row['language'];
		$tweets=intval($row['tweets']);
		$retweets=intval($row['retweets']);
		$likes=intval($row['likes']);
		
		$tweets_by_language[$language]=$tweets;
		$retweets_by_language[$language]=$retweets;
		$likes_by_language[$language]=$likes;
	}
	
	//Retrieve data from table twitter_user
	$sql="SELECT * FROM expertanalyzer.twitter_user WHERE username='".$twitter_username."'";
	$rows=mysqli_query($conn,$sql);
	
	$followers;
	while($row=mysqli_fetch_assoc($rows)){
		$followers=intval($row['followers']);
		
	}
	
	//Close DB connection
	mysqli_close($conn);
	
	$data['followers']=$followers;
	$data['tweets_by_language']=$tweets_by_language;
	$data['retweets_by_language']=$retweets_by_language;
	$data['likes_by_language']=$likes_by_language;
	
	
	
	
	return $data;
	
}

?>