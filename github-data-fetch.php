<?php
//Github Data Fetch***********************************************************
	
if($github_data){
	
	$_SESSION['github_username']=$github_username;
	$github_account=true;
	$github_followers = $github_data['followers'];
	$account_duration = $github_data['account_duration']; //in days
	$involvement_duration = $github_data['involvement_duration'];	//in days
	$github_repos_counter = $github_data['repositories'];
	$repos_by_language = $github_data['repos_by_language'];
	$commits_by_year = $github_data['commits_by_year'];
	
	
	if($repos_by_language){
		//Associative array keys.
		$commits_keys = array_keys($commits_by_year);
		$commits_languages_keys=array_keys($commits_by_year['2015']);
		//Total repositories of user.
		$github_repos_counter=0;
		//Associative array keys.
		$keys = array_keys($repos_by_language);
		
		for($i=0;$i<count($repos_by_language);$i++){
			$github_repos_counter +=intval($repos_by_language[$keys[$i]]);
		}
		
		
		
		
		//Calculate percentages for each project's language of total repos.
		$percentages = new ArrayObject($repos_by_language);
		//$percentages=$repos_by_language->getArrayCopy();
		if($github_repos_counter>0)
			for($i=0;$i<count($percentages);$i++){
				$percentages[$keys[$i]]=number_format(doubleval($percentages[$keys[$i]]/$github_repos_counter*100),2);
			}
		//Check if any commits exist.
		$github_commits_exist=false;
		foreach($commits_by_year as $k => $v){
			if($v>0){
				$github_commits_exist=true;
				break;
			}
				
		}
		
		//Check if any repos about a language exist.
		
		foreach($repos_by_language as $k => $v){
			if($v>0){
				$github_repos_exist=true;
				break;
			}
		}
		
		//Create Github graph js code*************************************************
		if($github_commits_exist){
			$_SESSION['github_graph']= "<script>$(function() {

			Morris.Area({
				element: 'morris-area-chart',
				data: [
				";
				for($i=0;$i<count($commits_keys);$i++){
					$_SESSION['github_graph'].=
					" {period: '".$commits_keys[$i]." ', ";
					for($j=0;$j<count($commits_languages_keys);$j++){
						$_SESSION['github_graph'].="'".$commits_languages_keys[$j]."' : ".$commits_by_year[$commits_keys[$i]][$commits_languages_keys[$j]];
						if(!($j==4 || $j==(count($commits_languages_keys)-1) )){
							$_SESSION['github_graph'].=",";
						}
					}
					$_SESSION['github_graph'].="
						  }";
					if($i!=(count($commits_keys)-1)){
						$_SESSION['github_graph'].=" , ";
					}
				}
				$_SESSION['github_graph'].="],
				xkey: 'period',
				ykeys: [";
				for($i=0;$i<count($commits_languages_keys);$i++){
					if($i==5)
						break;
					if($i==4 || $i==(count($commits_languages_keys)-1) )
						$_SESSION['github_graph'].="'".$commits_languages_keys[$i]."'";
					else
						$_SESSION['github_graph'].="'".$commits_languages_keys[$i]."',";
				}	
				$_SESSION['github_graph'].="],
				labels: [";
				for($i=0;$i<count($commits_languages_keys);$i++){
					if($i==5)
						break;
					if($i==4 || $i==(count($commits_languages_keys)-1) )
						$_SESSION['github_graph'].="'".$commits_languages_keys[$i]."'";
					else
						$_SESSION['github_graph'].="'".$commits_languages_keys[$i]."',";
				}
				$_SESSION['github_graph'].="],
				pointSize: 2,
				hideHover: 'auto',
				resize: true
			});
			});
			</script>";
		}
		if($github_repos_exist){
			$_SESSION['github_graph2']="<script>$(function() {
			Morris.Donut({
				element: 'morris-donut-chart',
				data: [";
				for($i=0;$i<count($percentages);$i++){
					if($percentages[$keys[$i]]==0){
						continue;
					}
					$_SESSION['github_graph2'].=" { label: '".$keys[$i]."', value: ".number_format($percentages[$keys[$i]],2)."  }";
					
					if($i<count($percentages)-1){
						$_SESSION['github_graph2'].=", ";
					}
					
				}
				$_SESSION['github_graph2'].="
					],
					 formatter: function (x) { return x + '%'},
				resize: true
			});
			});
			</script>";
			
			
		}
		if($github_repos_exist){
			$_SESSION['github_graph3']="<script>$(function() {
			Morris.Bar({
				element: 'repos-by-language-bar-chart',
				data: [";
				arsort($repos_by_language,true);
			
				$keys = array_keys($repos_by_language);
				
				for($i=0;$i<8;$i++){
					if($repos_by_language[$keys[$i]]==0){
						break;
					}
					$_SESSION['github_graph3'].=" { language: '".$keys[$i]."', count: ".$repos_by_language[$keys[$i]]." }";
					//Top 7 languages only.
					if($i<7){
						$_SESSION['github_graph3'].=", ";
					}
					
				}
				$_SESSION['github_graph3'].="],
				xkey: 'language',
				ykeys: ['count'],
				labels: ['Projects'],
				hideHover: 'auto',
				resize: true
			});

			});
			</script>";
		}
		//END Create Github graph js code**********************************************
		$_SESSION['repositories']=$github_repos_counter;
	}
	else{
		$_SESSION['repositories']=0;
	}
	$_SESSION['github_followers']=$github_followers;
	$_SESSION['account_duration']=$account_duration;
	$_SESSION['involvement_duration']=$involvement_duration;
	
}

	
	//End Github Data Fetch*******************************************************
?>